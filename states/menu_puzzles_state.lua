local menu_puzzles_state = {
    puzzle_scroll = gui.scroll_area {
        x = 64,
        y = 64,
        width = 300,
        height = 300,
        inner_height = 0,
    },
    puzzle_cols = 1,
    selected_puzzle = 0,
    is_selected = false,
    selection_timer = 0,

    deselect_button = gui.button {
        x = 0,
        y = 0,
        width = 32,
        height = 32,
        action = function(self)
            self.is_selected = false
            resources.sounds.shift2:play()
        end,
    },

    tint_1 = { 0.3, 0.75, 1, 1 },
    tint_2 = { 0.1, 0.05, 0.3, 1 },
}

function menu_puzzles_state.draw_puzzles(self, timer)
    for i,v in ipairs(self.puzzles) do
        if timer < 1 then
            local fac = math.max(math.min((timer - (i / #self.puzzles * 0.2)) + (timer - (i / #self.puzzles * 0.8)), 1), 0)
            v.button.scale = fac
        else
            v.button.scale = nil
        end

        v.button:draw()
    end
end

function menu_puzzles_state.update_puzzles(self, dt, x, y)
    if not x then
        for _,v in ipairs(self.puzzles) do
            v.button:update(dt, -100, -100)
        end
        return
    end

    for _,v in ipairs(self.puzzles) do
        v.button:update(dt, x, y)
    end
end

function menu_puzzles_state.load_puzzles(self, parent)
    for _,params in ipairs(resources.puzzles) do
        local icon = love.graphics.newImage(params.image)
        local puzzle_data = {
            icon = love.graphics.newImage(params.image),
            quad = love.graphics.newQuad(params.preview_region.x, params.preview_region.y, params.preview_region.width, params.preview_region.height, icon),
            title_label = love.graphics.newText(resources.fonts.default, params.title or "Untitled"),
            author_label = love.graphics.newText(resources.fonts.default, "by " .. (params.author or "Unknown")),
            params = params,
            sizes = {},
        }
        if params.desc then
            puzzle_data.desc_label = love.graphics.newText(resources.fonts.default, "\"" .. params.desc .. "\"")
        end
        local _,_,qw,_ = puzzle_data.quad:getViewport()
        puzzle_data.button = gui.button {
            x = 0,
            y = 0,
            width = 134,
            height = 134,
            content = puzzle_data.icon,
            content_scale = 128 / qw,
            quad = puzzle_data.quad,
            color = { 0.8, 0.8, 0.8 },
            pressed_color = { 0.6, 0.6, 0.6 },
            arg = { #self.puzzles + 1, self },
            action = function(data)
                if not data[2].is_selected then
                    resources.sounds.shift:play()
                end
                data[2].selected_puzzle = data[1]
                data[2].is_selected = true
            end
        }

        for i,size in ipairs(params.piece_sizes) do
            local w = math.floor(icon:getPixelWidth() / size.width)
            local h = math.floor(icon:getPixelHeight() / size.height)
            local index = #self.puzzles + 1
            local id = i
            local option = {
                id = id,
                button = gui.button {
                    x = -4,
                    y = -4,
                    content = love.graphics.newText(resources.fonts.default, tostring(w) .. "X" .. tostring(h)),
                    action = function()
                        parent.done = true
                        parent.puzzle_id = index
                        parent.size_id = id
                    end,
                }
            }

            option.button.width = 48
            option.button.height = 32
            table.insert(puzzle_data.sizes, option)
        end

        table.insert(self.puzzles, puzzle_data)
    end
end

function menu_puzzles_state.startup(self, parent)
    local _,window_height = self.get_viewport_dims()

    self.deselect_button.arg = self
    self.deselect_button.content = resources.textures.deselect_icon

    self.text = love.graphics.newText(resources.fonts.large, "SELECT A PUZZLE")

    self.puzzle_scroll.draw_content = self.draw_puzzles
    self.puzzle_scroll.update_content = function(_, dt, x, y) self.update_puzzles(self, dt, x, y) end

    self.puzzles = {}
    self:load_puzzles(parent)

    self.puzzle_scroll.height = window_height - self.puzzle_scroll.y
    self.puzzle_scroll.inner_height = #self.puzzles * 144 - 16
end

function menu_puzzles_state.init(self)
    local _,height = self.get_viewport_dims()
    self.puzzle_scroll.height = height - 68
    self.selected_puzzle = 0
    self.selection_timer = 0
end

function menu_puzzles_state.exit(self)
    self.is_selected = false
end

function menu_puzzles_state.mousepressed(self, x, y, button)
    for _,v in ipairs(self.puzzles) do
        v.button:mousepressed(button)
        for _,size in ipairs(v.sizes) do
            size.button:mousepressed(button)
        end
    end
    self.puzzle_scroll:mousepressed(x, y, button)
    if self.selected_puzzle ~= 0 then
        self.deselect_button:mousepressed(button)
    end
end

function menu_puzzles_state.mousereleased(self)
    self.puzzle_scroll:mousereleased()
end

function menu_puzzles_state.wheelmoved(self, xdiff, ydiff)
    self.puzzle_scroll:wheelmoved(xdiff, ydiff)
end

function menu_puzzles_state.resize(self, width, height)
    -- Resize the puzzle list
    self.puzzle_scroll.height = height - self.puzzle_scroll.y

    if width < 428 then
        self.puzzle_scroll.x = 16
        self.puzzle_scroll.width = width - 32
    else
        self.puzzle_scroll.x = 64
        self.puzzle_scroll.width = width - 128
    end
    self.puzzle_cols = math.floor(self.puzzle_scroll.width / 144)
    self.puzzle_scroll.height = height - 68
    self.puzzle_scroll.inner_height = (math.floor((#self.puzzles - 1) / self.puzzle_cols) + 1) * 144 - 8

    for i,v in ipairs(self.puzzles) do
        v.button.x = (i - 1) % self.puzzle_cols * 144
        v.button.y = math.floor((i - 1) / self.puzzle_cols) * 144
        if v.params.desc then
            local str_width = math.max(math.floor((width - 136) / 7), 1)
            v.desc_label:set(math.wrap_text("\"" .. v.params.desc .. "\"", str_width))
        end
        for ii,size in ipairs(v.sizes) do
            size.button.x = 136 + (ii - 1) * 48
        end
    end
    self.deselect_button.x = width - 40
end

function menu_puzzles_state.update(self, dt, mouse_x, mouse_y)
    local _,window_height = self.get_viewport_dims()

    self.deselect_button.y = 2 + window_height - (146 * self.selection_timer)
    self.deselect_button:update(dt, mouse_x, mouse_y)
    self.blocking_quit = mouse_y < 64 and self.deselect_button.hovered

    if not self.is_selected or (mouse_y < window_height - 130 and not self.deselect_button.hovered) then
        self.puzzle_scroll:update(dt, mouse_x, mouse_y)
    else
        self.puzzle_scroll:update(dt, -100, -100)
    end
    for i,puzzle in ipairs(self.puzzles) do
        for _,size in ipairs(puzzle.sizes) do
            local desc_height = 0
            if puzzle.desc_label then
                desc_height = puzzle.desc_label:getHeight()
            end
            if i == self.selected_puzzle then
                size.button.y = 48 + desc_height + window_height - (146 * self.selection_timer)
                size.button:update(dt, mouse_x, mouse_y)
            else
                size.button:update(dt, -100, -100)
            end
        end
    end

    if self.is_selected then
        self.selection_timer = math.min(self.selection_timer + dt * 4, 1)
    else
        self.selection_timer = math.max(self.selection_timer - dt * 4, 0)
    end
end

function menu_puzzles_state.transition_update(self, dt)
    local _,window_height = self.get_viewport_dims()

    self.deselect_button.y = 2 + window_height - (146 * self.selection_timer)
    for i,puzzle in ipairs(self.puzzles) do
        for _,size in ipairs(puzzle.sizes) do
            local desc_height = 0
            if puzzle.desc_label then
                desc_height = puzzle.desc_label:getHeight()
            end
            if i == self.selected_puzzle then
                size.button.y = 48 + desc_height + window_height - (146 * self.selection_timer)
            end
        end
    end

    self.selection_timer = math.max(self.selection_timer - dt * 4, 0)
end

function menu_puzzles_state.draw(self, timer)
    local window_width,window_height = self.get_viewport_dims()

    love.graphics.draw(self.text, (window_width - self.text:getWidth()) * 0.5, 21 - (64 * (1 - timer)))

    self.puzzle_scroll:draw(self, timer)
    if self.is_selected or self.selection_timer > 0 then
        love.graphics.push()
        love.graphics.translate(0, window_height - (146 * self.selection_timer * timer))
        resources.nineslices.info:draw(0, 0, window_width, 146)
        love.graphics.setColor(0, 0, 0)
        love.graphics.rectangle("fill", 3, 17, 128, 128)

        local puzzle = self.puzzles[self.selected_puzzle]
        love.graphics.setColor(1, 1, 1)
        love.graphics.draw(puzzle.icon, puzzle.quad, 1, 15, 0, puzzle.button.content_scale, puzzle.button.content_scale)
        love.graphics.draw(puzzle.title_label, 136, 16)
        love.graphics.draw(puzzle.author_label, 136, 32)
        if puzzle.desc_label then
            love.graphics.setColor(1, 201 / 255, 130 / 255)
            love.graphics.draw(puzzle.desc_label, 136, 48)
            love.graphics.setColor(1, 1, 1)
        end
        love.graphics.pop()

        for _,size in ipairs(puzzle.sizes) do
            size.button:draw()
        end

        self.deselect_button:draw()
    end
end

return menu_puzzles_state
