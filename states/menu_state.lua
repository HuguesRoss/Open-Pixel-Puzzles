local menu_state = {
    states = {
        main = require("states/menu_main_state"),
        puzzles = require("states/menu_puzzles_state"),
        settings = require("states/menu_settings_state"),
        credits = require("states/menu_credits_state"),
    },
    default_state = "main",
    current_state = "main",
    last_state = "main",
    state_transition_timer = 1.5,

    bg_timer = 0,
    current_tint_1 = { 0, 0.0, 0.0, 1 },
    current_tint_2 = { 0.0, 0.0, 0.0, 1 },

    back_button = gui.button {
        x = 16,
        y = 16,
        width = 32,
        height = 32,
        action = function(self)
            self:set_state(self.default_state)
        end,
    },
    quit_button = gui.button {
        x = 16,
        y = 16,
        width = 32,
        height = 32,
        action = function(self)
            self.done = true
        end,
    },
}

function menu_state.startup(self)
    self.bg = love.graphics.newQuad(0, 0, 256, 256, 32, 32)

    self.back_button.arg = self
    self.back_button.content = resources.textures.back_icon

    self.quit_button.arg = self
    self.quit_button.content = resources.textures.quit_icon

    for _,v in pairs(self.states) do
        v.get_viewport_dims = self.get_viewport_dims
        v:startup(self)
    end
end

function menu_state.init(self)
    self.done = false
    self.puzzle_id = nil

    if self.states[self.current_state].init then
        self.states[self.current_state]:init()
    end

    local window_width,window_height = self.get_viewport_dims()
    self:resize(window_width, window_height)

    self.set_music(self:get_music())
end

function menu_state.get_music()
    return resources.music.menu[love.math.random(1, #resources.music.menu)]
end

function menu_state.set_state(self, state)
    if self.states[self.current_state].exit then
        self.states[self.current_state]:exit()
    end

    self.last_state = self.current_state
    self.current_state = state
    self.state_transition_timer = 0

    if self.states[self.current_state].init then
        self.states[self.current_state]:init()
    end
end

function menu_state.mousepressed(self, x, y, button)
    self.quit_button:mousepressed(button)

    if self.states[self.current_state].mousepressed then
        self.states[self.current_state]:mousepressed(x, y, button)
    end

    if self.current_state ~= self.default_state then
        self.back_button:mousepressed(button)
    end
end

function menu_state.mousereleased(self, _, _)
    if self.states[self.current_state].mousereleased then
        self.states[self.current_state]:mousereleased()
    end
end

function menu_state.wheelmoved(self, _, _, xdiff, ydiff)
    if self.states[self.current_state].wheelmoved then
        self.states[self.current_state]:wheelmoved(xdiff, ydiff)
    end
end

function menu_state.resize(self, width, height)
    self.quit_button.x = width - self.quit_button.width - 16

    for _,v in pairs(self.states) do
        v:resize(width, height)
    end
end

function menu_state.update(self, dt, mouse_x, mouse_y)
    self.bg_timer = self.bg_timer + dt
    self.state_transition_timer = math.min(self.state_transition_timer + dt * 2, 1.5)
    if self.done then
        if self.states[self.current_state].exit then
            self.states[self.current_state]:exit()
        end
        if self.puzzle_id == nil then
            return "quit"
        else
            return "puzzle", { params = self.states.puzzles.puzzles[self.puzzle_id].params, size = self.size_id }
        end
    end

    if self.last_state ~= self.current_state and self.state_transition_timer < 1 then
        gui.in_transition = true
        if self.states[self.last_state].transition_update then
            self.states[self.last_state]:transition_update(dt)
        end
    end

    local current_state = self.states[self.current_state]
    current_state:update(dt, mouse_x, mouse_y)
    for i=1,3 do
        self.current_tint_1[i] = (self.current_tint_1[i] * 0.9) + (current_state.tint_1[i] * 0.1)
        self.current_tint_2[i] = (self.current_tint_2[i] * 0.9) + (current_state.tint_2[i] * 0.1)
    end

    if not current_state.blocking_quit then
        self.quit_button:update(dt, mouse_x, mouse_y)
    else
        self.quit_button:update(dt, -100, -100)
    end

    if self.current_state ~= self.default_state then
        if not current_state.blocking_back then
            self.back_button:update(dt, mouse_x, mouse_y)
        else
            self.back_button:update(dt, -100, -100)
        end
    end
end

function menu_state.draw(self)
    local window_width,window_height = self.get_viewport_dims()

    love.graphics.push()
    love.graphics.scale(4, 4)
    love.graphics.rotate(0.2)
    love.graphics.setShader(resources.shaders.bg_shader)
    resources.shaders.bg_shader:send("time", self.bg_timer * 0.75)
    resources.shaders.bg_shader:send("h_tex", resources.textures.bg3)
    resources.shaders.bg_shader:send("v_tex", resources.textures.bg2)
    resources.shaders.bg_shader:send("tint1", self.current_tint_1)
    resources.shaders.bg_shader:send("tint2", self.current_tint_2)
    local x,y = -256,-256
    while y < window_height do
        while x < window_width do
            love.graphics.draw(resources.textures.bg, self.bg, x, y)
            x = x + 256
        end
        x = 0
        y = y + 256
    end
    love.graphics.setShader()
    love.graphics.pop()

    self.quit_button:draw()

    -- Back button
    love.graphics.push()
    if self.current_state ~= self.default_state then
        love.graphics.translate(0, -64 * math.max((1 - self.state_transition_timer - 0.5) * 2, 0))
        self.back_button:draw()
    elseif self.last_state ~= self.default_state then
        love.graphics.translate(0, -64 * math.max(self.state_transition_timer * 2, 0))
        self.back_button:draw()
    end
    love.graphics.pop()

    if self.last_state ~= self.current_state and self.state_transition_timer < 1 then
        self.states[self.last_state]:draw(1 - self.state_transition_timer)
    end
    if self.state_transition_timer > 0.5 then
        self.states[self.current_state]:draw(self.state_transition_timer - 0.5)
    end
end

return menu_state
