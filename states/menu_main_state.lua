local menu_main_state = {
    main_menu = {
        { "PLAY", function(self)
            self:set_state("puzzles")
        end },
        -- { "PUZZLE CREATOR" },
        { "SETTINGS", function(self)
            self:set_state("settings")
        end },
        { "CREDITS", function(self)
            self:set_state("credits")
        end },
    },
    tint_1 = { 1, 0.75, 0.0, 1 },
    tint_2 = { 0.1, 0.0, 0.3, 1 },
}

function menu_main_state.startup(self, parent)
    for i,v in ipairs(self.main_menu) do
        v.button = gui.button {
            x = 0,
            y = 100 + (i - 1) * 40,
            width = 128,
            height = 32,
            content = love.graphics.newText(resources.fonts.default, v[1]),
            action = v[2],
            arg = parent,
        }
    end
end

function menu_main_state.mousepressed(self, _, _, button)
    for _,v in ipairs(self.main_menu) do
        v.button:mousepressed(button)
    end
end

function menu_main_state.resize(self, _, height)
    local title_offset = 100
    local menu_offset = 40
    if height > 500 then
        menu_offset = 60
        if height > 600 then
            title_offset = 200
        end
    elseif height < 250 then
        title_offset = 80
        menu_offset = 34
    end
    local menu_height = menu_offset * #self.main_menu
    for i,v in ipairs(self.main_menu) do
        v.button.y = math.floor(title_offset + (height - title_offset - menu_height) * 0.35 + (i - 1) * menu_offset)
    end
end

function menu_main_state.update(self, dt, mouse_x, mouse_y)
    local window_width,_ = self.get_viewport_dims()
    local half_width = window_width * 0.5 - 64
    for _,v in ipairs(self.main_menu) do
        v.button:update(dt, mouse_x - half_width, mouse_y)
    end
end

function menu_main_state.draw(self, timer)
    local window_width,window_height = self.get_viewport_dims()
    local half_width = window_width * 0.5 - 64

    love.graphics.push()
    love.graphics.translate(0, -window_height * (1 - timer))

    local title_y = math.floor(math.max(self.main_menu[1].button.y - 96, 0) * 0.5)

    love.graphics.draw(resources.textures.title, half_width - 32, title_y)

    love.graphics.push()
    love.graphics.translate(half_width, 0)
    for _,v in ipairs(self.main_menu) do
        v.button:draw()
    end
    love.graphics.pop()
    love.graphics.pop()
end

return menu_main_state
