local menu_credits_state = {
    credits_scroll = gui.scroll_area {
        x = 64,
        y = 64,
        width = 300,
        height = 300,
        inner_height = 0,
    },
    credits_lines = {
        ["CONCEPT"] = {
            "Hugues Alexandre Ross"
        },
        ["PUZZLES"] = {},
        ["UI"] = {
            "Hugues Alexandre Ross"
        },
        ["BGM"] = { },
        ["SE"] = {
            {
                title = "button.wav",
                author = "Hugues Alexandre Ross",
                license = { "by", "sa" }
            },
            {
                title = "pick.wav",
                orig = "click1.wav",
                author = "florian_reinke",
                license = "zero"
            },
            {
                title = "pick2.wav",
                orig = "click1.wav",
                author = "florian_reinke",
                license = "zero"
            },
            {
                title = "place.wav",
                orig = "click.wav",
                author = "Ranner",
                license = "zero"
            },
            {
                title = "puzzle_complete.ogg",
                author = "ShareAnecdotes",
                license = { "by", "sa" },
            },
            {
                title = "shift.wav",
                orig = "chalk1.wav",
                author = "P_Chester",
                license = "zero"
            },
            {
                title = "shuffle.wav",
                orig = "Paper Manipulation On Glass",
                author = "hasbrouck",
                license = "by"
            },
            {
                title = "transition.wav",
                author = "Hugues Alexandre Ross",
                license = { "by", "sa" }
            },
        },
    },
    credits_str = "",
    credits_order = {
        "CONCEPT",
        "PUZZLES",
        "UI",
        "BGM",
        "SE"
    },
    tint_1 = { 0.7, 0.4, 0.5, 1 },
    tint_2 = { 0.0, 0.05, 0.15, 1 },
}

function menu_credits_state.draw_credits(self, _)
    for _,v in ipairs(self.credits_text) do
        if v:getFont() == resources.fonts.large then
            love.graphics.translate(0, 16)
        end
        love.graphics.draw(v)
        love.graphics.translate(0, v:getHeight())
    end
end

function menu_credits_state.startup(self)
    self.text = love.graphics.newText(resources.fonts.large, "CREDITS")

    -- Add puzzle info to the credits data
    for _,v in pairs(resources.puzzles) do
        table.insert(self.credits_lines["PUZZLES"], v)
    end

    -- Add BGM info to the credits data
    for _,v in pairs(resources.music.menu) do
        table.insert(self.credits_lines["BGM"], v)
    end
    for _,v in pairs(resources.music.ingame) do
        table.insert(self.credits_lines["BGM"], v)
    end

    -- Construct credits text
    self.credits_text = {}
    for _,k in ipairs(self.credits_order) do
        table.insert(self.credits_text, love.graphics.newText(resources.fonts.large, k))
        local credits_str = ""
        for _,line in ipairs(self.credits_lines[k]) do
            if type(line) == "string" then
                -- Simple line
                credits_str = credits_str .. line .. "\n"
            else
                -- File authorship data
                credits_str = credits_str .. line.title .. ": "
                if line.orig then
                    credits_str = credits_str .. "Edited from " .. line.orig .. " by " .. line.author .. "\n"
                else
                    credits_str = credits_str .. line.author .. "\n"
                end
            end
        end
        table.insert(self.credits_text, love.graphics.newText(resources.fonts.default, credits_str))
    end

    for _,v in ipairs(self.credits_text) do
        self.credits_scroll.inner_height = self.credits_scroll.inner_height + v:getHeight()
        if v:getFont() == resources.fonts.large then
            self.credits_scroll.inner_height = self.credits_scroll.inner_height + 16
        end
    end

    self.credits_scroll.draw_content = self.draw_credits
end

function menu_credits_state.resize(self, width, height)
    if width < 428 then
        self.credits_scroll.x = 16
        self.credits_scroll.width = width - 32
    else
        self.credits_scroll.x = 64
        self.credits_scroll.width = width - 128
    end
    self.credits_scroll.height = height - self.credits_scroll.y

    local i = 1
    self.credits_scroll.inner_height = 0
    for _,k in ipairs(self.credits_order) do
        self.credits_scroll.inner_height = self.credits_scroll.inner_height + self.credits_text[i]:getHeight() + 16
        i = i + 1
        local credits_str = ""
        for _,line in ipairs(self.credits_lines[k]) do
            if type(line) == "string" then
                -- Simple line
                credits_str = credits_str .. line .. "\n"
            else
                -- File authorship data
                credits_str = credits_str .. line.title .. ": "
                if line.orig then
                    credits_str = credits_str .. "Edited from " .. line.orig .. " by " .. line.author .. "\n"
                else
                    credits_str = credits_str .. line.author .. "\n"
                end
            end
        end
        self.credits_text[i]:set(math.wrap_text(credits_str, self.credits_scroll.width / 7))
        self.credits_scroll.inner_height = self.credits_scroll.inner_height + self.credits_text[i]:getHeight()
        i = i + 1
    end
end

function menu_credits_state.update(self, dt, mouse_x, mouse_y)
    self.credits_scroll:update(dt, mouse_x, mouse_y)
end

function menu_credits_state.draw(self, timer)
    local window_width,window_height = self.get_viewport_dims()

    love.graphics.push()
    love.graphics.translate(0, window_height * (1 - timer))
    self.credits_scroll:draw(self, timer)
    love.graphics.pop()

    love.graphics.draw(self.text, (window_width - self.text:getWidth()) * 0.5, 21 - (64 * (1 - timer)))
end

function menu_credits_state.mousepressed(self, x, y, button)
    self.credits_scroll:mousepressed(x, y, button)
end

function menu_credits_state.mousereleased(self)
    self.credits_scroll:mousereleased()
end

function menu_credits_state.wheelmoved(self, xdiff, ydiff)
    self.credits_scroll:wheelmoved(xdiff, ydiff)
end

return menu_credits_state
