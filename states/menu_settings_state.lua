local menu_settings_state = {
    settings_menu = {},

    settings_scroll = gui.scroll_area {
        x = 64,
        y = 64,
        width = 300,
        height = 300,
        inner_height = 20 + 32 + 24, -- Reset button offset, height, bpttom pad
    },
    reset_settings_button = gui.button {
        x = 0,
        y = 0,
        width = 64,
        height = 32,
        action = function(self)
            settings.reset()
            self:sync_settings()
        end,
    },

    tint_1 =  { 0.7, 1, 0.3, 1 },
    tint_2 =  { 0.0, 0.1, 0.3, 1 },
}

function menu_settings_state.draw_settings(self)
    love.graphics.translate(32, 0)
    for _,v in ipairs(self.settings_menu) do
        if v.gui then
            love.graphics.draw(v.label, 0, 5)
            love.graphics.push()
            love.graphics.translate(96, 0)
            v.gui:draw()
            love.graphics.pop()
        else
            love.graphics.translate(0, 16)
            love.graphics.draw(v.label, 0, 5)
        end

        love.graphics.translate(0, v.height)
    end

    love.graphics.translate(0, 20)
    self.reset_settings_button:draw()
end

function menu_settings_state.sync_settings(self)
    for _,v in ipairs(self.settings_menu) do
        if v.gui then
            local def = settings.defs_by_id[v.id]

            if def.type == "number" or def.type == "boolean" then
                v.gui:set_value(settings[v.id])
            end
        end
    end
end

function menu_settings_state.update_settings(self, dt, x, y)
    if not x then
        for _,v in ipairs(self.settings_menu) do
            if v.gui then
                v.gui:update(dt, -100, -100)
            end
        end
        self.reset_settings_button:update(dt, -100, -100)
    else
        for _,v in ipairs(self.settings_menu) do
            if v.gui then
                v.gui:update(dt, x - 128, y)
            else
                y = y - 16
            end
            y = y - v.height
        end

        y = y - 20
        self.reset_settings_button:update(dt, x - 32, y)
    end
end

function menu_settings_state.startup(self, _)
    local _,window_height = self.get_viewport_dims()

    self.reset_settings_button.arg = self

    self.text = love.graphics.newText(resources.fonts.large, "SETTINGS")

    self.reset_settings_button.content = love.graphics.newText(resources.fonts.default, "RESET");
    self.settings_scroll.height = window_height - self.settings_scroll.y
    self.settings_scroll.draw_content = function() self.draw_settings(self) end
    self.settings_scroll.update_content = function(_, dt, x, y) self.update_settings(self, dt, x, y) end

    for _,group in ipairs(settings.definitions) do
        local group_label = {
            label = love.graphics.newText(resources.fonts.large, group.name),
            height = 24,
        }
        table.insert(self.settings_menu, group_label)
        self.settings_scroll.inner_height = self.settings_scroll.inner_height + 40

        for _,setting in ipairs(group) do
            local menu_item = {
                id = setting.id,
                label = love.graphics.newText(resources.fonts.default, setting.name),
                height = 32,
            }
            self.settings_scroll.inner_height = self.settings_scroll.inner_height + 32
            if setting.type == "number" then
                menu_item.gui = gui.slider {
                    x = 0,
                    y = 4,
                    width = 128,
                    height = 16,
                    min = setting.min,
                    max = setting.max,
                    value = settings[setting.id],
                    action = function(value)
                        settings[setting.id] = value
                    end
                }
            elseif setting.type == "boolean" then
                menu_item.gui = gui.checkbox {
                    x = 7,
                    y = 0,
                    width = 24,
                    height = 24,
                    min = setting.min,
                    max = setting.max,
                    value = settings[setting.id],
                    action = function(value)
                        settings[setting.id] = value
                    end
                }
            end
            table.insert(self.settings_menu, menu_item)
        end
    end
end

function menu_settings_state.init(self)
    local _,height = self.get_viewport_dims()
    self.settings_scroll.height = height - self.settings_scroll.y
end

function menu_settings_state.exit(_)
    settings.save()
end

function menu_settings_state.mousepressed(self, x, y, button)
    for _,v in ipairs(self.settings_menu) do
        if v.gui then
            v.gui:mousepressed(button)
        end
    end
    self.reset_settings_button:mousepressed(button)
    self.settings_scroll:mousepressed(x, y, button)
end

function menu_settings_state.mousereleased(self)
    for _,v in ipairs(self.settings_menu) do
        if v.gui and v.gui.mousereleased then
            v.gui:mousereleased()
        end
    end
    self.settings_scroll:mousereleased()
end

function menu_settings_state.wheelmoved(self, xdiff, ydiff)
    self.settings_scroll:wheelmoved(xdiff, ydiff)
end

function menu_settings_state.resize(self, width, height)
    self.settings_scroll.x = math.max((width - self.settings_scroll.width) * 0.5, 32)
    self.settings_scroll.y = 64
    self.settings_scroll.height = height - self.settings_scroll.y
end

function menu_settings_state.update(self, dt, mouse_x, mouse_y)
    self.settings_scroll:update(dt, mouse_x, mouse_y)
end

function menu_settings_state.draw(self, timer)
    local window_width,_ = self.get_viewport_dims()

    love.graphics.draw(self.text, (window_width - self.text:getWidth()) * 0.5, 21 - (64 * (1 - timer)))

    love.graphics.push()
    love.graphics.translate(window_width * (1 - timer), 0)

    love.graphics.setColor(0, 0.03, 0.18, 0.5)
    love.graphics.rectangle("fill", self.settings_scroll.x, self.settings_scroll.y, self.settings_scroll.width, math.min(self.settings_scroll.height, self.settings_scroll.inner_height))
    love.graphics.setColor(1,1,1)
    self.settings_scroll:draw()
    love.graphics.pop()
end

return menu_settings_state
