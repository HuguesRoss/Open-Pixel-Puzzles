local puzzle_state = {
    selected = 0,
    selected_group = 0,
    hovered = 0,
    hovered_group = 0,
    snapped_slot_ids = {},

    view_x = 0,
    view_y = 0,
    scale = 1,

    group_offset_x = 0,
    group_offset_y = 0,

    scene_left = 0,
    scene_top = 0,
    scene_right = 0,
    scene_bottom = 0,

    startup_timer = 10,
    startup_tick = -1,

    back_button = gui.button {
        x = 16,
        y = 16,
        width = 32,
        height = 32,
    },
}

function puzzle_state.startup(self)
    self.back_button.content = resources.textures.back_icon
    self.back_button.action = function()
        self.back = true
        resources.sounds.complete:stop()
    end
end

function puzzle_state.init(self, data)
    self.selected = 0
    self.selected_group = 0
    self.hovered = 0
    self.hovered_group = 0
    self.snapped_slot_ids = {}

    self.view_x = 0
    self.view_y = 0
    self.scale = 1

    local window_width,window_height = self.get_viewport_dims()
    self.scene_left = -window_width * 0.5
    self.scene_right = window_width * 0.5
    self.scene_top = -window_height * 0.5
    self.scene_bottom = window_height * 0.5

    self.back = false

    self.puzzle, self.scene_left, self.scene_right, self.scene_top, self.scene_bottom = puzzle.new(data.params, data.size, self.scene_left, self.scene_right, self.scene_top, self.scene_bottom)
    if data.params.bg_color then
        love.graphics.setBackgroundColor(data.params.bg_color)
    else
        love.graphics.setBackgroundColor(0.5, 0.5, 0.5)
    end
    self.startup_timer = 4 + (0.035 * #self.puzzle.pieces)
    self.startup_tick = self.startup_timer - 3

    self.set_music(self:get_music())
end

function puzzle_state.get_music()
    return resources.music.ingame[love.math.random(1, #resources.music.ingame)]
end

function puzzle_state.keypressed(self, key, _, _)
    if debug_mode.enabled and key == "s" then
        for _,v in ipairs(self.puzzle.pieces) do
            self.puzzle:place_piece(v.id, v.id)
        end
        self.puzzle:validate()
        if self.puzzle.solved then
            self.set_music(nil)
        end
    end
end

function puzzle_state.mousepressed(self, x, y, button)
    self.back_button:mousepressed(button)

    if button == 3 then
        self.scroll_start_x = x
        self.scroll_start_y = y
    end

    if self.hovered ~= 0 and self.startup_timer <= 0 then
        local piece = self.puzzle.pieces[self.hovered]
        if button == 1 then -- LMB, lift
            love.audio.stop(resources.sounds.pick2)
            love.audio.play(resources.sounds.pick2)
            table.remove(self.puzzle.pieces, self.hovered)
            table.insert(self.puzzle.pieces, piece)
            self.selected = piece.id
            self.selected_group = piece.group_id
            self.group_offset_x = -piece.group_x
            self.group_offset_y = -piece.group_y
            if self.selected_group == 0 then
                self.puzzle:lift_pieces({ piece.id })
            else
                self.puzzle:lift_pieces(self.puzzle.groups[self.selected_group].pieces)
            end
        elseif button == 2 then -- RMB, lift and break group
            love.audio.play(resources.sounds.pick2)
            self.selected = piece.id
            if piece.group_id ~= 0 then
                self.puzzle:break_group(piece.group_id, piece.id)
            end
            self.puzzle:lift_pieces({ piece.id })
        end
    end
end

function puzzle_state.mousereleased(self, _, _, button)
    if button == 3 then
        self.scroll_start_x = nil
        self.scroll_start_y = nil
    else -- Left or right button
        if self.selected == 0 then
            return
        end

        self.group_offset_x = 0
        self.group_offset_y = 0

        if #self.snapped_slot_ids > 0 then
            debug_mode.print("PLACE PIECE(S)")
            love.audio.play(resources.sounds.place)
            if self.selected_group == 0 then
                self.puzzle:place_piece(self.selected, self.snapped_slot_ids[1])
            else
                local pieces = self.puzzle.groups[self.selected_group].pieces
                for k,v in ipairs(self.snapped_slot_ids) do
                    self.puzzle:place_piece(pieces[k], v)
                end
            end
        end

        self.puzzle:validate()
        if self.puzzle.solved then
            self.set_music(nil)
            resources.sounds.complete:play()
        end
        self.selected = 0
        self.selected_group = 0
    end
end

function puzzle_state.wheelmoved(self, x, y, _, ydiff)
    if (self.scale == 1 and ydiff < 0) or (self.scale == 8 and ydiff > 0) then
        return
    end

    local window_width,window_height = self.get_viewport_dims()
    x = (x - window_width * 0.5) / self.scale
    y = (y - window_height * 0.5) / self.scale

    self.scale = self.scale + ydiff

    if self.scale < 1 then
        self.scale = 1
    elseif self.scale > 8 then
        self.scale = 8
    end

    if ydiff > 0 then
        self.view_x = self.view_x - x
        self.view_y = self.view_y - y
    end
end

function puzzle_state.resize(self, width, height)
    -- Update scene extents
    self.scene_left = -width * 0.5
    self.scene_right = width * 0.5
    self.scene_top = -height * 0.5
    self.scene_bottom = height * 0.5

    -- Expand the scene if the puzzle doesn't fit
    if self.scene_left > self.puzzle.canvas_left then
        self.scene_left = self.scene_left + self.puzzle.canvas_left
        self.scene_right = self.scene_right + self.puzzle.canvas_right
    end
    if self.scene_top > self.puzzle.canvas_top then
        self.scene_top = self.scene_top + self.puzzle.canvas_top
        self.scene_bottom = self.scene_bottom + self.puzzle.canvas_bottom
    end

    -- Shift puzzle pieces into bounds
    for _,piece in ipairs(self.puzzle.pieces) do
        if not piece.placed then
            local moved = false
            if piece.x < self.scene_left + self.puzzle.tile_width * 0.2 then
                piece.x = self.scene_left + self.puzzle.tile_width * 0.2
                moved = true
            elseif piece.x > self.scene_right - self.puzzle.tile_width * 1.2 then
                piece.x = self.scene_right - self.puzzle.tile_width * 1.2
                moved = true
            end
            if piece.y < self.scene_top + self.puzzle.tile_height * 0.2 then
                piece.y = self.scene_top + self.puzzle.tile_height * 0.2
                moved = true
            elseif piece.y > self.scene_bottom - self.puzzle.tile_height * 1.2 then
                piece.y = self.scene_bottom - self.puzzle.tile_height * 1.2
                moved = true
            end

            -- Shift pieces out from under the back button
            if piece.x < self.scene_left + 64 and piece.y < self.scene_top + 64 then
                local diff_x = self.scene_left + 64 - piece.x
                local diff_y = self.scene_top + 64 - piece.y
                piece.x = piece.x + math.min(diff_x, diff_y)
                piece.y = piece.y + math.min(diff_x, diff_y)
                moved = true
            end

            -- Add some random offsets to the shifted pieces to reduce stacking
            if moved then
                piece.x = piece.x + love.math.random(-self.puzzle.tile_width * 0.2, self.puzzle.tile_width * 0.2)
                piece.y = piece.y + love.math.random(-self.puzzle.tile_height * 0.2, self.puzzle.tile_height * 0.2)
            end
        end
    end
end

function puzzle_state.update(self, dt, x, y)
    if self.back then
        return "menu"
    end

    if self.startup_timer > 0 then
        self.startup_timer = self.startup_timer - dt
        if self.startup_timer < self.startup_tick - 0.035 and self.startup_timer > 1 then
            if resources.sounds.shuffle:isPlaying() then
                resources.sounds.shuffle:stop()
            end
            resources.sounds.shuffle:play()
            self.startup_tick = self.startup_timer
        end
    end

    self.back_button:update(dt, x, y)

    self.puzzle:update(dt)

    if self.puzzle.solved and self.puzzle.solved_timer > 10 then
        self.back = true
    end

    local window_width,window_height = self.get_viewport_dims()

    if self.scroll_start_x then
        self.view_x = self.view_x + x - self.scroll_start_x
        self.view_y = self.view_y + y - self.scroll_start_y
        self.scroll_start_x = x
        self.scroll_start_y = y
    end

    -- Clamp the view to the edges of the scene
    if self.view_x + (window_width * 0.5 / self.scale) > self.scene_right then
        self.view_x = self.scene_right - (window_width * 0.5 / self.scale)
    elseif self.view_x - (window_width * 0.5 / self.scale) < self.scene_left then
        self.view_x = self.scene_left + (window_width * 0.5 / self.scale)
    end
    if self.view_y + (window_height * 0.5 / self.scale) > self.scene_bottom then
        self.view_y = self.scene_bottom - (window_height * 0.5 / self.scale)
    elseif self.view_y - (window_height * 0.5 / self.scale) < self.scene_top then
        self.view_y = self.scene_top + (window_height * 0.5 / self.scale)
    end

    -- Piece handling
    x = (x - window_width * 0.5) / self.scale - self.view_x
    y = (y - window_height * 0.5) / self.scale - self.view_y

    local last_hovered = self.hovered
    local last_hovered_group = self.hovered_group
    self.hovered = 0
    self.hovered_group = 0
    self.snapped_slot_ids = {}
    for id,piece in ipairs(self.puzzle.pieces) do
        if piece.id == self.selected or (self.selected_group ~= 0 and piece.group_id == self.selected_group) then
            local cursor_x = x + piece.group_x + self.group_offset_x
            local cursor_y = y + piece.group_y + self.group_offset_y
            piece.x = cursor_x - (self.puzzle.tile_width * 0.5)
            piece.y = cursor_y - (self.puzzle.tile_height * 0.5)
        elseif self.selected == 0 and
            x > piece.x and x < piece.x + self.puzzle.tile_width and
            y > piece.y and y < piece.y + self.puzzle.tile_height then
            self.hovered = id
            self.hovered_group = piece.group_id
        end
    end

    if not self.puzzle.solved and self.startup_timer <= 0 and self.hovered ~= 0 and last_hovered ~= self.hovered and (last_hovered_group == 0 or last_hovered_group ~= self.hovered_group) then
        if not self.puzzle.pieces[self.hovered].placed then
            love.audio.stop(resources.sounds.pick)
            love.audio.play(resources.sounds.pick)
        end
    end

    if self.selected ~= 0 then
        if self.selected_group == 0 then
            local piece = self.puzzle:getpiece(self.selected)
            local cursor_x = x + piece.group_x + self.group_offset_x
            local cursor_y = y + piece.group_y + self.group_offset_y
            for slot_id,slot in ipairs(self.puzzle.slots) do
                if math.abs(cursor_x - slot.x) < self.puzzle.tile_width and math.abs(cursor_y - slot.y) < self.puzzle.tile_height then
                    if self.puzzle:piecefits(slot_id, slot, piece) then
                        self.snapped_slot_ids = { slot_id }
                        piece.x = slot.x
                        piece.y = slot.y
                    end
                    break
                end
            end
        else
            local can_snap = true
            local slots = {}
            for k,v in ipairs(self.puzzle.groups[self.selected_group].pieces) do
                slots[k] = 0

                local piece = self.puzzle:getpiece(v)
                local cursor_x = x + piece.group_x + self.group_offset_x
                local cursor_y = y + piece.group_y + self.group_offset_y

                for slot_id,slot in ipairs(self.puzzle.slots) do
                    if math.abs(cursor_x - slot.x) < self.puzzle.tile_width and math.abs(cursor_y - slot.y) < self.puzzle.tile_height then
                        if self.puzzle:piecefits(slot_id, slot, piece) then
                            slots[k] = slot_id
                        end
                        break
                    end
                end

                if slots[k] == 0 then
                    can_snap = false
                    break
                end
            end

            if can_snap then
                self.snapped_slot_ids = slots
                for k,v in ipairs(self.puzzle.groups[self.selected_group].pieces) do
                    local piece = self.puzzle:getpiece(v)
                    local slot = self.puzzle.slots[slots[k]]
                    piece.x = slot.x
                    piece.y = slot.y
                end
            end
        end
    end
end

function puzzle_state.animate_piece(self, piece)
    local start_x = (piece.id - 1) % self.puzzle.width * self.puzzle.tile_width + self.puzzle.canvas_left
    local start_y = math.floor((piece.id - 1) / self.puzzle.width) * self.puzzle.tile_height + self.puzzle.canvas_top
    local x = start_x + (piece.x - start_x) * math.min(math.max(1 - (self.startup_timer - (#self.puzzle.pieces - piece.id) * 0.035), 0) * 3, 1)
    local y = start_y + (piece.y - start_y) * math.min(math.max(1 - (self.startup_timer - (#self.puzzle.pieces - piece.id) * 0.035), 0) * 3, 1)

    return x, y
end

function puzzle_state.draw(self)
    love.graphics.push()
    local window_width,window_height = self.get_viewport_dims()
    love.graphics.translate(window_width * 0.5, window_height * 0.5)
    love.graphics.scale(self.scale)
    love.graphics.translate(self.view_x, self.view_y)

    self.puzzle:draw()

    love.graphics.setColor(1, 1, 1)
    love.graphics.setShader(resources.shaders.piece_shader)
    if self.startup_timer > 0 then
        -- Draw placed pieces
        for id,piece in ipairs(self.puzzle.pieces) do
            if self.startup_timer - (#self.puzzle.pieces - id) * 0.035 > 1 then
                local x,y = self:animate_piece(piece)
                self.puzzle:draw_piece(piece, x, y)
            end
        end

        -- Draw moving pieces
        for id,piece in ipairs(self.puzzle.pieces) do
            if self.startup_timer - (#self.puzzle.pieces - id) * 0.035 <= 1 then
                local x,y = self:animate_piece(piece)
                self.puzzle:draw_piece(piece, x, y, true)
            end
        end
    else
        for id,piece in ipairs(self.puzzle.pieces) do
            if not piece.placed then
                if piece.id == self.selected or id == self.hovered or (self.hovered_group ~= 0 and piece.group_id == self.hovered_group) or (self.selected_group ~= 0 and piece.group_id == self.selected_group) then
                    love.graphics.setColor(1, 1, 1, 0.8)
                else
                    love.graphics.setColor(1, 1, 1)
                end

                self.puzzle:draw_piece(piece, piece.x, piece.y, true)
            end
        end
    end
    love.graphics.setShader()
    love.graphics.pop()

    love.graphics.setColor(1,1,1)
    self.back_button:draw()

    if self.puzzle.solved and self.puzzle.solved_timer > 2.25 then
        local fac = math.min((self.puzzle.solved_timer - 2.25) * 3, 1)
        local y = math.lerp(window_height * 0.6, window_height * 0.5 - 16, fac)
        love.graphics.setColor(1, 1, 1, fac)
        love.graphics.draw(resources.textures.complete, window_width * 0.5 - 128, y)
        love.graphics.setColor(1, 1, 1)
    end
end

return puzzle_state
