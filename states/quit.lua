local quit_state = {
}

function quit_state.init(_)
    love.event.quit()
end

function quit_state.draw(_) end

return quit_state
