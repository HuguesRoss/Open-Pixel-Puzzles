uniform float time;
uniform vec4 tint1;
uniform vec4 tint2;
uniform Image h_tex;
uniform Image v_tex;

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    vec4 texturecolor = Texel(tex, texture_coords);
    float min_value = 0.6;
    float diff_value = 0.1;
    vec2 screen_offset = vec2(Texel(h_tex, texture_coords).r, Texel(v_tex, texture_coords).r);
    float fac = (sin(screen_coords.y * 0.0125) * screen_offset.y * 0.1) + (sin(screen_coords.x * 0.0125 + 5) * screen_offset.x * 0.1) + sin(time + (texturecolor.r * 3) + (texturecolor.g * 7) + (texturecolor.b * 11)) * diff_value;
    return ((fac + min_value) * tint1) + ((1 - fac + min_value) * tint2);
}
