uniform float time;

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    vec4 texturecolor = Texel(tex, texture_coords);
    texturecolor.r = max(texturecolor.r, 0.01);
    float time2 = max(time - 1, 0);
    float fac = ceil(max((1-texturecolor.r)-(1 - min(time, 1)), 0)) - ceil(max(texturecolor.r-(1 - time2), 0));
    return vec4(0, 0, 0, fac);
}
