attribute vec2 SimpleTexCoord;

varying vec2 mask_coords;

vec4 position(mat4 transform_projection, vec4 vertex_position)
{
    mask_coords = SimpleTexCoord;
    return transform_projection * vertex_position;
}
