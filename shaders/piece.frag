uniform Image mask;
varying vec2 mask_coords;
uniform vec4 add_color;

vec4 effect(vec4 color, Image tex, vec2 texture_coords, vec2 screen_coords)
{
    vec4 texturecolor = Texel(tex, texture_coords) + add_color;
    texturecolor *= Texel(mask, mask_coords);
    return texturecolor * color;
}
