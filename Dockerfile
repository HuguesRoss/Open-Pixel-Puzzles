FROM nickblah/lua:5.4-luarocks-alpine

RUN apk add --no-cache build-base jpeg-dev python3 python3-dev py3-pip zlib-dev
RUN luarocks install luacheck
RUN pip3 install makelove
