local nineslice_methods = {
    draw = function(self, x, y, w, h)
        local x_scale = 1
        if w < self.slice_w * 2 then
            x_scale = w / (self.slice_w * 2)
        end
        local y_scale = 1
        if h < self.slice_h * 2 then
            y_scale = h / (self.slice_h * 2)
        end

        love.graphics.draw(self.image, self.quads[1], x, y, 0, x_scale, y_scale) -- Top-left
        if w > self.slice_w * 2 then
            love.graphics.draw(self.image, self.quads[2], x + self.slice_w, y, 0, math.ceil((w - (self.slice_w * 2)) / self.slice_w), 1) -- Top
        end
        love.graphics.draw(self.image, self.quads[3], x + (w - self.slice_w) * x_scale, y, 0, x_scale, y_scale) -- Top-right
        if h > self.slice_h * 2 then
            love.graphics.draw(self.image, self.quads[4], x, y + self.slice_h, 0, 1, math.ceil((h - (self.slice_h * 2)) / self.slice_h)) -- Left
            if w > self.slice_w * 2 then
                love.graphics.draw(self.image, self.quads[5], x + self.slice_w, y + self.slice_h, 0, math.ceil((w - (self.slice_w * 2)) / self.slice_w), math.ceil((h - (self.slice_h * 2)) / self.slice_h)) -- Center
            end
            love.graphics.draw(self.image, self.quads[6], x + w - self.slice_w, y + self.slice_h, 0, 1, math.ceil((h - (self.slice_h * 2)) / self.slice_h)) -- Right
        end
        love.graphics.draw(self.image, self.quads[7], x, y + (h - self.slice_h) * y_scale, 0, x_scale, y_scale) -- Bottom-left
        if w > self.slice_w * 2 then
            love.graphics.draw(self.image, self.quads[8], x + self.slice_w, y + h - self.slice_h, 0, math.ceil((w - self.slice_w * 2) / self.slice_w), 1) -- Bottom
        end
        love.graphics.draw(self.image, self.quads[9], x + (w - self.slice_w) * x_scale, y + (h - self.slice_h) * y_scale, 0, x_scale, y_scale) -- Bottom-right
    end,
}

if not gui then
    gui = {}
end

gui.nineslice = function(args)
    setmetatable(args, { __index = nineslice_methods, })
    args.offset_x = args.offset_x or 0
    args.offset_y = args.offset_y or 0
    args.quads = {
        love.graphics.newQuad(args.offset_x + (args.slice_w * 0), args.offset_y + (args.slice_h * 0), args.slice_w, args.slice_h, args.image),
        love.graphics.newQuad(args.offset_x + (args.slice_w * 1), args.offset_y + (args.slice_h * 0), args.slice_w, args.slice_h, args.image),
        love.graphics.newQuad(args.offset_x + (args.slice_w * 2), args.offset_y + (args.slice_h * 0), args.slice_w, args.slice_h, args.image),
        love.graphics.newQuad(args.offset_x + (args.slice_w * 0), args.offset_y + (args.slice_h * 1), args.slice_w, args.slice_h, args.image),
        love.graphics.newQuad(args.offset_x + (args.slice_w * 1), args.offset_y + (args.slice_h * 1), args.slice_w, args.slice_h, args.image),
        love.graphics.newQuad(args.offset_x + (args.slice_w * 2), args.offset_y + (args.slice_h * 1), args.slice_w, args.slice_h, args.image),
        love.graphics.newQuad(args.offset_x + (args.slice_w * 0), args.offset_y + (args.slice_h * 2), args.slice_w, args.slice_h, args.image),
        love.graphics.newQuad(args.offset_x + (args.slice_w * 1), args.offset_y + (args.slice_h * 2), args.slice_w, args.slice_h, args.image),
        love.graphics.newQuad(args.offset_x + (args.slice_w * 2), args.offset_y + (args.slice_h * 2), args.slice_w, args.slice_h, args.image),
    }

    return args
end
