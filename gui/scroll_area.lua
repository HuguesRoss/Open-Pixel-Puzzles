local scroll_area_methods = {
    update = function(self, dt, mouse_x, mouse_y)
        if self.disabled then
            return
        end

        if not self.horizontal then
            local fraction = self.height * self.height / self.inner_height
            local offset = self.height * self.scroll_y / self.inner_height
            if mouse_x > self.x + self.width - 8 and mouse_x < self.x + self.width and
            mouse_y > self.y + offset and mouse_y < self.y + offset + fraction then
                self.hovered = true
            else
                self.hovered = false
            end

            if self.pressed then
                self.scroll_y = (((mouse_y - self.y) / self.height) + self.pressed_fraction) * self.inner_height
            end
        else
            local fraction = self.width * self.width / self.inner_width
            local offset = self.width * self.scroll_x / self.inner_width
            if mouse_y > self.y + self.height - 8 and mouse_y < self.y + self.height and
            mouse_x > self.x + offset and mouse_x < self.x + offset + fraction then
                self.hovered = true
            else
                self.hovered = false
            end

            if self.pressed then
                self.scroll_x = (((mouse_x - self.x) / self.width) + self.pressed_fraction) * self.inner_width
            end
        end

        self.scroll_x = math.max(0, math.min(self.scroll_x, self.inner_width - self.width))
        self.scroll_y = math.max(0, math.min(self.scroll_y, self.inner_height - self.height))

        if not self.update_content then
            return
        end

        if mouse_x > self.x and mouse_x < self.x + self.width and
           mouse_y > self.y and mouse_y < self.y + self.height then
            self.update_content(self, dt, mouse_x - self.x + self.scroll_x, mouse_y - self.y + self.scroll_y)
        else
            self.update_content(self, dt, nil, nil)
        end
    end,

    mousepressed = function(self, x, y, button)
        if self.hovered and button == 1 then
            self.pressed = true
            if not self.horizontal then
                self.pressed_fraction = (self.scroll_y / self.inner_height) - ((y - self.y) / self.height)
            else
                self.pressed_fraction = (x - self.x) / self.width
            end
        end
    end,

    mousereleased = function(self)
        self.pressed = false
    end,

    wheelmoved = function(self, xdiff, ydiff)
        self.scroll_x = math.max(0, math.min(self.scroll_x - xdiff * 16, self.inner_width - self.width))
        self.scroll_y = math.max(0, math.min(self.scroll_y - ydiff * 16, self.inner_height - self.height))
    end,

    draw = function(self, ...)
        local r,g,b,a = love.graphics.getColor()

        love.graphics.push()
        love.graphics.translate(self.x, self.y)
        love.graphics.stencil(self.stencil, "replace", 1)
        love.graphics.setStencilTest("greater", 0)

        love.graphics.setColor(r,g,b,a)

        love.graphics.push()
        love.graphics.translate(-self.scroll_x, -self.scroll_y)
        if self.draw_content then
            self.draw_content(...)
        end
        love.graphics.pop()

        -- Scrollbars
        local slice = resources.nineslices.scrollbar
        if self.pressed then
            slice = resources.nineslices.scrollbar_press
        elseif self.hovered then
            slice = resources.nineslices.scrollbar_hover
        elseif self.disabled then
            slice = resources.nineslices.scrollbar_disabled
        end
        if not self.horizontal then
            if self.height <= self.inner_height then
                resources.nineslices.slider:draw(self.width - 8, 0, 8, self.height)
                local fraction = self.height / self.inner_height
                local offset = self.scroll_y / self.inner_height
                if fraction < 1 then
                    slice:draw(self.width - 8, self.height * offset, 8, self.height * fraction)
                end
            end
        else
            if self.width <= self.inner_width then
                resources.nineslices.slider:draw(0, self.height - 8, self.width, 8)
                local fraction = (self.inner_width - self.width) / self.inner_width
                local offset = self.scroll_x / self.inner_width
                if fraction < 1 then
                    slice:draw(self.width * offset, self.height - 8, self.width * fraction, 8)
                end
            end
        end
        love.graphics.pop()
        love.graphics.setStencilTest()
    end,
}

if not gui then
    gui = {}
end

gui.scroll_area = function(args)
    setmetatable(args, { __index = scroll_area_methods, })
    args.stencil = function()
        love.graphics.setColor(0, 0, 0)
        love.graphics.rectangle("fill", 0, 0, args.width, args.height)
    end


    args.inner_width = args.inner_width or args.width
    args.inner_height = args.inner_height or args.height
    args.scroll_x = args.scroll_x or 0
    args.scroll_y = args.scroll_y or 0

    return args
end
