local checkbox_methods = {
    update = function(self, dt, mouse_x, mouse_y)
        if mouse_x > self.x and mouse_x < self.x + self.width and
           mouse_y > self.y and mouse_y < self.y + self.height and self.action then
           if not self.hovered and not gui.in_transition then
               resources.sounds.pick:stop()
               resources.sounds.pick:play()
           end
            self.hovered = true
            if self.hovertime < 1 then
                self.hovertime = math.min(self.hovertime + dt * 10, 1)
            end
        else
            self.hovered = false
            if self.hovertime > 0 then
                self.hovertime = math.max(self.hovertime - dt * 10, 0)
            end
        end

        if self.pressed then
            self.presstime = self.presstime + dt

            if self.presstime > 0.25 then
                self.pressed = false
                self.presstime = 0
                self.value = not self.value
                self.action(self.value)
            end
        end
    end,

    draw = function(self)
        local r,g,b,a = love.graphics.getColor()

        local w = self.width * (self.scale or 1)
        local h = self.height * (self.scale or 1)
        local x = self.x + (self.width - w) * 0.5
        local y = self.y + (self.height - h) * 0.5

        love.graphics.setColor(1, 1, 1)
        local set_color = false
        if self.pressed and self.presstime < 0.15 then
            resources.nineslices.checkbox_press:draw(x, y, w, h)
            if self.pressed_color then
                love.graphics.setColor(self.pressed_color)
                set_color = true
            end
        elseif self.disabled then
            resources.nineslices.checkbox_disabled:draw(x, y, w, h)
        elseif self.hovered then
            resources.nineslices.checkbox_hover:draw(x, y, w, h)
            if self.hovered_color then
                love.graphics.setColor(self.hovered_color)
                set_color = true
            end
        else
            resources.nineslices.checkbox:draw(x, y, w, h)
            if self.color then
                love.graphics.setColor(self.color)
                set_color = true
            end
        end

        if not set_color then
            love.graphics.setColor(r, g, b, a)
        end

        if self.value == true then
            local q_index = 1
            if self.disabled then
                q_index = 4
            elseif self.pressed and self.presstime < 0.15 then
                q_index = 3
            elseif self.hovered then
                q_index = 2
            end
            local quad = self.quads[q_index]

            local scale = (self.scale or 1)
            local _,_,cw,ch = quad:getViewport()
            love.graphics.draw(resources.textures.checkbox_image, quad, x + (self.width - cw) * 0.5 * (self.scale or 1), y + (self.height - ch) * 0.5 * (self.scale or 1), 0, scale, scale)
        end
        love.graphics.setColor(r, g, b, a)
    end,

    mousepressed = function(self, checkbox)
        if self.hovered and checkbox == 1 then
            self.pressed = true
            resources.sounds.button:stop()
            resources.sounds.button:play()
        end
    end,

    set_value = function(self, value)
        self.value = value
    end
}

if not gui then
    gui = {}
end

gui.checkbox = function(args)
    setmetatable(args, { __index = checkbox_methods, })
    args.hovered = false
    args.pressed = false
    args.hovertime = 0
    args.presstime = 0
    args.quads = {
        love.graphics.newQuad(48, 0, 24, 24, resources.textures.checkbox_image),
        love.graphics.newQuad(72, 0, 24, 24, resources.textures.checkbox_image),
        love.graphics.newQuad(48, 24, 24, 24, resources.textures.checkbox_image),
        love.graphics.newQuad(72, 24, 24, 24, resources.textures.checkbox_image),
    }

    return args
end
