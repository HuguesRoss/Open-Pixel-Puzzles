local button_methods = {
    update = function(self, dt, mouse_x, mouse_y)
        if mouse_x > self.x and mouse_x < self.x + self.width and
           mouse_y > self.y and mouse_y < self.y + self.height and self.action then
           if not self.hovered and not gui.in_transition then
               resources.sounds.pick:stop()
               resources.sounds.pick:play()
           end
            self.hovered = true
            if self.hovertime < 1 then
                self.hovertime = math.min(self.hovertime + dt * 10, 1)
            end
        else
            self.hovered = false
            if self.hovertime > 0 then
                self.hovertime = math.max(self.hovertime - dt * 10, 0)
            end
        end

        if self.pressed then
            self.presstime = self.presstime + dt

            if self.presstime > 0.25 then
                self.pressed = false
                self.presstime = 0
                self.action(self.arg)
            end
        end
    end,

    draw = function(self)
        local r,g,b,a = love.graphics.getColor()

        local w = self.width * (self.scale or 1)
        local h = self.height * (self.scale or 1)
        local x = self.x + (self.width - w) * 0.5
        local y = self.y + (self.height - h) * 0.5

        local content_offset = 0
        love.graphics.setColor(1, 1, 1)
        local set_color = false
        if self.pressed and self.presstime < 0.15 then
            resources.nineslices.button_press:draw(x, y, w, h)
            content_offset = 1
            if self.pressed_color then
                love.graphics.setColor(self.pressed_color)
                set_color = true
            end
        elseif not self.action then
            resources.nineslices.button_disabled:draw(x, y, w, h)
            content_offset = 1
        elseif self.hovered then
            resources.nineslices.button_hover:draw(x, y, w, h)
            if self.hovered_color then
                love.graphics.setColor(self.hovered_color)
                set_color = true
            end
        else
            resources.nineslices.button:draw(x, y, w, h)
            if self.color then
                love.graphics.setColor(self.color)
                set_color = true
            end
        end

        if not set_color then
            love.graphics.setColor(r, g, b, a)
        end

        if self.content then
            local cw = self.content:getWidth() * (self.content_scale or 1)
            local ch = self.content:getHeight() * (self.content_scale or 1)
            local scale = (self.scale or 1) * (self.content_scale or 1)
            if self.quad then
                local _
                _,_,cw,ch = self.quad:getViewport()
                cw = cw * (self.content_scale or 1)
                ch = ch * (self.content_scale or 1)
                love.graphics.draw(self.content, self.quad, x + (self.width - cw) * 0.5 * (self.scale or 1), y + (self.height - ch) * 0.5 * (self.scale or 1) + content_offset, 0, scale, scale)
            else
                love.graphics.draw(self.content, x + (self.width - cw) * 0.5 * (self.scale or 1), y + (self.height - ch) * 0.5 * (self.scale or 1) + content_offset, 0, scale, scale)
            end
        end
        love.graphics.setColor(r, g, b, a)
    end,

    mousepressed = function(self, button)
        if self.hovered and button == 1 then
            self.pressed = true
            resources.sounds.button:stop()
            resources.sounds.button:play()
        end
    end,
}

if not gui then
    gui = {}
end

gui.button = function(args)
    setmetatable(args, { __index = button_methods, })
    args.hovered = false
    args.pressed = false
    args.hovertime = 0
    args.presstime = 0

    return args
end
