local slider_normal_quad = love.graphics.newQuad(0, 0, 16, 16, 32, 32)
local slider_hover_quad = love.graphics.newQuad(16, 0, 16, 16, 32, 32)
local slider_press_quad = love.graphics.newQuad(0, 16, 16, 16, 32, 32)
-- local slider_disabled_quad = love.graphics.newQuad(16, 16, 16, 16, 32, 32)

local slider_methods = {
    update = function(self, dt, mouse_x, mouse_y)
        if self.pressed then
            if mouse_x < 0  or mouse_y < 0 then
                return
            end

            local fraction = -1
            if mouse_x > self.x + self.width then
                fraction = 1
            elseif mouse_x > self.x then
                fraction = (mouse_x - self.x) / self.width
            end
            fraction = math.min(math.max(fraction, 0), 1)
            self:set_value(math.floor(self.min + (fraction * (self.max - self.min)) + 0.5))
        elseif mouse_x > self.x + (self.fraction * (self.width - self.height)) and
               mouse_x < self.x + (self.fraction * (self.width - self.height)) + self.height and
               mouse_y > self.y and mouse_y < self.y + self.height then
            if not self.hovered and not gui.in_transition then
                resources.sounds.pick:stop()
                resources.sounds.pick:play()
            end
            self.hovered = true
            if self.hovertime < 1 then
                self.hovertime = math.min(self.hovertime + dt * 10, 1)
            end
        else
            self.hovered = false
            if self.hovertime > 0 then
                self.hovertime = math.max(self.hovertime - dt * 10, 0)
            end
        end
    end,

    draw = function(self)
        love.graphics.draw(self.label, self.x + self.width, self.y)

        resources.nineslices.slider:draw(self.x + (self.height * 0.5), self.y + (self.height * 0.25), self.width - self.height, self.height * 0.5)

        if self.pressed then
            love.graphics.draw(resources.textures.slider_knobs, slider_press_quad, self.x + (self.fraction * (self.width - self.height)), self.y)
        elseif self.hovered then
            love.graphics.draw(resources.textures.slider_knobs, slider_hover_quad, self.x + (self.fraction * (self.width - self.height)), self.y)
        else
            love.graphics.draw(resources.textures.slider_knobs, slider_normal_quad, self.x + (self.fraction * (self.width - self.height)), self.y)
        end
    end,

    mousepressed = function(self, button)
        if self.hovered and button == 1 then
            if not self.pressed and not gui.in_transition then
                resources.sounds.pick:stop()
                resources.sounds.pick:play()
            end
            self.pressed = true
        end
    end,

    mousereleased = function(self)
        if self.pressed then
            if not gui.in_transition then
                resources.sounds.pick2:stop()
                resources.sounds.pick2:play()
            end
            self.pressed = false
            if self.action then
                self.action(self.value, self.arg)
            end
        end
    end,

    set_value = function(self, value)
        self.value = value
        self.fraction = (self.value - self.min) / (self.max - self.min)
        self.label:set(tostring(self.value))
    end
}

if not gui then
    gui = {}
end

gui.slider = function(args)
    setmetatable(args, { __index = slider_methods, })
    args.hovered = false
    args.pressed = false
    args.hovertime = 0
    if not args.min then
        args.min = 0
    end
    if not args.max then
        args.max = args.min + 1
    end
    if not args.value then
        args.value = (args.max - args.min) * 0.5
    end
    args.fraction = (args.value - args.min) / (args.max - args.min)
    args.label = love.graphics.newText(resources.fonts.default, tostring(args.value))

    return args
end
