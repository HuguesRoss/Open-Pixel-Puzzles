local serializer = {}

function serializer.load_params(filepath)
    local params = {}
    if not love.filesystem.getInfo(filepath, "file") then
        return params, "File not found"
    end

    for line in love.filesystem.lines(filepath) do
        local _,_,k,v = line:find("(.+)=(.+)")
        params[k] = v
    end

    return params
end

function serializer.save_params(filepath, params)
    local data = ""

    for k,v in pairs(params) do
        data = data .. k .. "=" .. v .. "\n"
    end

    -- Remove the trailing \n
    data = data:sub(1, #data - 1)

    debug_mode.print(string.format("Writing to %s:\n%s", filepath, data))
    return love.filesystem.write(filepath, data)
end

return serializer
