function math.clamp(x, min, max)
    return math.min(max, math.max(min, x))
end

function math.lerp(a, b, f)
    return a + (b - a) * f
end

function math.lerpcolor(r1, g1, b1, r2, g2, b2, f)
    return math.lerp(r1, r2, f), math.lerp(g1, g2, f), math.lerp(b1, b2, f)
end

function math.tocolor(str)
    local r,g,b = str:match("#(%x%x)(%x%x)(%x%x)")
    return tonumber("0x" .. r) / 255, tonumber("0x" .. g) / 255, tonumber("0x" .. b) / 255
end

function math.wrap_text(str, width)
    if #str < width then
        return str
    end

    local output = ""
    local i = 1
    while i < #str do
        local next_line = str:find("\n", i, true)
        if next_line and next_line < i + width then
            output = output .. str:sub(i, next_line)
            i = next_line + 1
        else
            output = output .. str:sub(i, i + width - 1)
            if i + width < #str then
                local space_index = output:find(" ")
                while space_index do
                    local tmp = output:find(" ", space_index + 1)
                    if tmp then
                        space_index = tmp
                    else
                        break
                    end
                end

                if space_index and #output - space_index < width then
                    output = output:sub(1, space_index - 1)
                    i = space_index - width
                end
                output = output .. "\n"
            end
            i = i + width
        end
    end

    return output
end
