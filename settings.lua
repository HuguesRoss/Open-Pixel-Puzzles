settings = {
    data = {},
    definitions = {
        { id = "graphics", name = "GRAPHICS",
            { id = "pixel_scale", name = "PIXEL SCALE",
                type = "number",
                min = 1,
                max = 3,
                default = 2,
            },
            { id = "fullscreen", name = "FULLSCREEN",
                type = "boolean",
                default = false,
                on_set = function(value)
                    love.window.setFullscreen(value)
                end,
            }
            -- { id = "theme", name = "THEME", -- TODO
            --     type = "string",
            --     default = "deault",
            -- },
        },
        { id = "audio", name = "AUDIO",
            { id = "sound_volume", name = "SOUND VOLUME",
                type = "number",
                min = 0,
                max = 100,
                default = 50,
                on_set = function(value)
                    for _,v in pairs(resources.sounds) do
                        v:setVolume(value * 0.01)
                    end
                end,
            },
            { id = "music_volume", name = "MUSIC VOLUME",
                type = "number",
                min = 0,
                max = 100,
                default = 50,
                on_set = function(value)
                    for _,v in pairs(resources.music.menu) do
                        v.source:setVolume(value * 0.01)
                    end
                    for _,v in pairs(resources.music.ingame) do
                        v.source:setVolume(value * 0.01)
                    end
                end,
            },
        },
        { id = "gameplay", name = "GAMEPLAY",
            { id = "puzzle_overlay", name = "PUZZLE OVERLAY",
                type = "boolean",
                default = true,
            }
        },
    },
    defs_by_id = {},
    settings_file = "settings.conf",
}

for _,group in ipairs(settings.definitions) do
    for _,setting in ipairs(group) do
        settings.defs_by_id[setting.id] = setting
    end
end

function settings.reset()
    for k,v in pairs(settings.defs_by_id) do
        settings[k] = v.default
    end
end

function settings.save()
    debug_mode.print("Saving settings")
    local settings_data = {}
    for k,_ in pairs(settings.defs_by_id) do
        settings_data[k] = tostring(settings[k])
    end

    local success,err = serializer.save_params(settings.settings_file, settings_data)
    if not success then
        -- TODO: GUI error reporting
        print("Failed to write settings: " .. err)
    end
end

setmetatable(settings, {
    __index = function(self, k)
        return self.data[k]
    end,
    __newindex = function(self, k, v)
        if self.defs_by_id[k] then
            rawset(self.data, k, v)

            if self.defs_by_id[k].on_set then
                self.defs_by_id[k].on_set(v)
            end
        end
    end
})

local function parse_field(value, def)
    if def.type == "number" then
        return math.clamp(tonumber(value), def.min, def.max)
    elseif def.type == "boolean" then
        return value == "true"
    else
        -- TODO: GUI error reporting
        print("Can't parse fields of type " .. def.type)
    end
end

debug_mode.print("Loading settings")
local params,err = serializer.load_params(settings.settings_file)
if not err then
    for k,v in pairs(params) do
        if settings[k] == nil and settings.defs_by_id[k] ~= nil then
            settings[k] = parse_field(v, settings.defs_by_id[k])
        end
    end
    debug_mode.print("Done loading settings")
else
    -- TODO: GUI error reporting
    debug_mode.print("Failed to retrieve settings from " .. settings.settings_file .. ": " .. err)
    debug_mode.print("Reverting to defaults")
end

for k,v in pairs(settings.defs_by_id) do
    if settings[k] == nil then
        settings[k] = v.default
    end
end
