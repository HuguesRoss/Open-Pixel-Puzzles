new_globals = {
    "debug_mode",
    "resources",
    "serializer",
    "gui",
    "puzzle",
    "settings",
    "states",

    -- From mathutil.lua
    "math.clamp",
    "math.lerp",
    "math.lerpcolor",
    "math.tocolor",
    "math.wrap_text",
}
