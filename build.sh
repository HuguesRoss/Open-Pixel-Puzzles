#!/bin/sh

VERSION=""
if [ $# -gt 1 ]; then
    VERSION="-n $2"
    echo VERSION $2
fi

if [ "$1" = "win64" ]; then
    export WINEPREFIX="$HOME/.wine-love"
    export WINEARCH="win64"
    cat makelove.toml makelove.win64.toml > makelove.combined.toml
    makelove --config makelove.combined.toml win64 $VERSION
fi

if [ "$1" = "appimage" ]; then
    cat makelove.toml makelove.appimage.toml > makelove.combined.toml
    makelove --config makelove.combined.toml appimage $VERSION
fi
