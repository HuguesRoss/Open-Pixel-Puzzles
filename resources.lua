resources = {
    nineslices = { },
    fonts = { },
    music = {
        menu = { },
        ingame = { },
    },
    puzzles = { },
    shaders = { },
    sounds = { },
    sound_data = { },
    textures = { },
}

function resources.init(folder)
    local resolved_folder = "skins/" .. folder .. "/"
    resources.fonts.default = love.graphics.newImageFont(resolved_folder .. "font.png", " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~\n")
    resources.fonts.large = love.graphics.newImageFont(resolved_folder .. "font_large.png", " !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ\n")

    resources.shaders.bg_shader = love.graphics.newShader("shaders/bg.vert", "shaders/bg.frag")
    resources.shaders.transition_shader = love.graphics.newShader("shaders/bg.vert", "shaders/transition.frag")
    resources.shaders.piece_shader = love.graphics.newShader("shaders/piece.vert", "shaders/piece.frag")

    resources.textures.title = love.graphics.newImage(resolved_folder .. "title.png")
    resources.textures.complete = love.graphics.newImage(resolved_folder .. "complete.png")
    resources.textures.back_icon = love.graphics.newImage(resolved_folder .. "back.png")
    resources.textures.deselect_icon = love.graphics.newImage(resolved_folder .. "deselect.png")
    resources.textures.music_icon = love.graphics.newImage(resolved_folder .. "music.png")
    resources.textures.quit_icon = love.graphics.newImage(resolved_folder .. "quit.png")
    resources.textures.bg = love.graphics.newImage(resolved_folder .. "bg_02.png")
    resources.textures.bg:setWrap("repeat", "repeat")
    resources.textures.bg2 = love.graphics.newImage(resolved_folder .. "bg_03.png")
    resources.textures.bg2:setWrap("repeat", "repeat")
    resources.textures.bg3 = love.graphics.newImage(resolved_folder .. "bg_04.png")
    resources.textures.bg3:setWrap("repeat", "repeat")
    resources.textures.checkbox_image = love.graphics.newImage(resolved_folder .. "checkbox_slices.png")
    resources.textures.transition = love.graphics.newImage(resolved_folder .. "transition_05.png")
    resources.textures.slider_knobs = love.graphics.newImage(resolved_folder .. "slider_knobs.png")

    resources.sound_data.transition = love.sound.newSoundData(resolved_folder .. "transition.wav")
    resources.sounds.button = love.audio.newSource(resolved_folder .. "button.wav", "static")
    resources.sounds.complete = love.audio.newSource(resolved_folder .. "puzzle_complete.ogg", "static")
    resources.sounds.place = love.audio.newSource(resolved_folder .. "place.wav", "static")
    resources.sounds.pick = love.audio.newSource(resolved_folder .. "pick.wav", "static")
    resources.sounds.pick2 = love.audio.newSource(resolved_folder .. "pick2.wav", "static")
    resources.sounds.shift = love.audio.newSource(resolved_folder .. "shift.wav", "static")
    resources.sounds.shift2 = love.audio.newSource(resolved_folder .. "shift2.wav", "static")
    resources.sounds.shuffle = love.audio.newSource(resolved_folder .. "shuffle.wav", "static")
    for _,v in pairs(resources.sounds) do
        v:setVolume(settings.sound_volume * 0.01)
    end

    local button_image = love.graphics.newImage(resolved_folder .. "button_slices.png")
    resources.nineslices.button = gui.nineslice {
        offset_x = 0,
        offset_y = 0,
        slice_w = 16,
        slice_h = 16,
        image = button_image,
    }
    resources.nineslices.button_hover = gui.nineslice {
        offset_x = 48,
        offset_y = 0,
        slice_w = 16,
        slice_h = 16,
        image = button_image,
    }
    resources.nineslices.button_press = gui.nineslice {
        offset_x = 0,
        offset_y = 48,
        slice_w = 16,
        slice_h = 16,
        image = button_image,
    }
    resources.nineslices.button_disabled = gui.nineslice {
        offset_x = 48,
        offset_y = 48,
        slice_w = 16,
        slice_h = 16,
        image = button_image,
    }

    resources.nineslices.checkbox = gui.nineslice {
        offset_x = 0,
        offset_y = 0,
        slice_w = 8,
        slice_h = 8,
        image = resources.textures.checkbox_image,
    }
    resources.nineslices.checkbox_hover = gui.nineslice {
        offset_x = 24,
        offset_y = 0,
        slice_w = 8,
        slice_h = 8,
        image = resources.textures.checkbox_image,
    }
    resources.nineslices.checkbox_press = gui.nineslice {
        offset_x = 0,
        offset_y = 24,
        slice_w = 8,
        slice_h = 8,
        image = resources.textures.checkbox_image,
    }
    resources.nineslices.checkbox_disabled = gui.nineslice {
        offset_x = 24,
        offset_y = 24,
        slice_w = 8,
        slice_h = 8,
        image = resources.textures.checkbox_image,
    }
    resources.nineslices.slider = gui.nineslice {
        slice_w = 4,
        slice_h = 4,
        image = love.graphics.newImage(resolved_folder .. "slider_slice.png"),
    }

    local scrollbar_image = love.graphics.newImage(resolved_folder .. "scrollbar_slices.png")
    resources.nineslices.scrollbar = gui.nineslice {
        slice_w = 4,
        slice_h = 4,
        image = scrollbar_image,
    }
    resources.nineslices.scrollbar_hover = gui.nineslice {
        offset_x = 12,
        offset_y = 0,
        slice_w = 4,
        slice_h = 4,
        image = scrollbar_image,
    }
    resources.nineslices.scrollbar_press = gui.nineslice {
        offset_x = 0,
        offset_y = 12,
        slice_w = 4,
        slice_h = 4,
        image = scrollbar_image,
    }
    resources.nineslices.scrollbar_disabled = gui.nineslice {
        offset_x = 12,
        offset_y = 12,
        slice_w = 4,
        slice_h = 4,
        image = scrollbar_image,
    }

    resources.nineslices.info = gui.nineslice {
        slice_w = 16,
        slice_h = 16,
        image = love.graphics.newImage(resolved_folder .. "info_slice.png"),
    }

    table.insert(resources.music.menu, {
        title = "LaDaDa Guitar",
        author = "Alex McCulloch",
        source = love.audio.newSource("music/LaDaDaGuitar.ogg", "stream"),
        license = "zero" ,
    })
    table.insert(resources.music.menu, {
        title = "One Step at a Time",
        author = "Alex McCulloch",
        source = love.audio.newSource("music/OneStepAtATime.ogg", "stream"),
        license = "zero",
    })
    table.insert(resources.music.ingame, {
        title = "Frets",
        author = "Alex McCulloch",
        source = love.audio.newSource("music/Frets.ogg", "stream"),
        license = { "zero" },
    })
    table.insert(resources.music.ingame, {
        title = "Sinking Feeling",
        author = "Jesse Spillane",
        source = love.audio.newSource("music/Jesse Spillane - Sinking Feeling.ogg", "stream"),
        license = "by",
    })
    table.insert(resources.music.ingame, {
        title = "In a Sentimental Mood",
        author = "Joe Reynolds - Professorlamp",
        source = love.audio.newSource("music/in a sentimental mood.ogg", "stream"),
        license = "by",
    })
    table.insert(resources.music.ingame, {
        title = "The Narrative Changes",
        author = "Revolution Void",
        source = love.audio.newSource("music/Revolution Void - The Narrative Changes.ogg", "stream"),
        license = "by",
    })
    table.insert(resources.music.ingame, {
        title = "Breakdown",
        author = "Mid-Air Machine",
        source = love.audio.newSource("music/Mid-Air Machine - Breakdown.ogg", "stream"),
        license = { "by", "sa" },
    })
    for _,v in pairs(resources.music.menu) do
        v.source:setVolume(settings.music_volume * 0.01)
    end
    for _,v in pairs(resources.music.ingame) do
        v.source:setVolume(settings.music_volume * 0.01)
    end

    resources.load_puzzles()
end

function resources.load_puzzles()
    for _,v in ipairs(love.filesystem.getDirectoryItems("puzzles")) do
        if love.filesystem.getInfo("puzzles/" .. v, "directory") then
            local params = puzzle.load_params("puzzles/" .. v)
            if params then
                params.title = params.title or "Untitled"
                params.author = params.author or "Unknown"
                table.insert(resources.puzzles, params)
            end
        end
    end
end
