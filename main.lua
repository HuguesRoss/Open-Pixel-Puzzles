-- Set to true for additional debug information
debug_mode = {
    enabled = false,
    print = function(str)
        if debug_mode.enabled then
            print(tostring(str))
        end
    end,
}

require("gui.9slice")
require("gui.button")
require("gui.checkbox")
require("gui.scroll_area")
require("gui.slider")
require("mathutil")
serializer = require("serializer")
require("puzzle")
require("resources")
require("settings")

gui.in_transition = false

states = {
    menu = require("states/menu_state"),
    puzzle = require("states/puzzle_state"),
    quit = require("states/quit"),
}
local current_state = "menu"

local transition = {
    timer = 0,
    active = false,
}

local current_music = nil
local music_change_timer = 0
local music_text = nil

local global_fns = {}
function global_fns.get_viewport_dims()
    local width,height = love.window.getMode()
    return width / settings.pixel_scale, height / settings.pixel_scale
end
function global_fns.set_music(music)
    if current_music == music then
        return
    end

    if current_music then
        current_music.source:stop()
    end

    current_music = music

    if current_music then
        music_change_timer = 0
        music_text:set(current_music.author .. " - " .. current_music.title)
        current_music.source:play()
    end
end

for _,v in pairs(states) do
    for k,fn in pairs(global_fns) do
        if not v[k] then
            v[k] = fn
        end
    end
end

local old_w = 0
local old_h = 0

function love.load()
    love.graphics.setDefaultFilter("nearest", "nearest")
    love.graphics.setBackgroundColor(0.5, 0.5, 0.5)
    resources.init("default")

    music_text = love.graphics.newText(resources.fonts.default, "Artist - Track Name")

    old_w,old_h = global_fns.get_viewport_dims()

    for _,state in pairs(states) do
        if state.startup then
            state:startup()
        end
    end

    states[current_state]:init()
    debug_mode.print("Done load")
end

function love.mousepressed(x, y, button)
    if states[current_state].mousepressed then
        states[current_state]:mousepressed(x / settings.pixel_scale, y / settings.pixel_scale, button)
    end
end

function love.keypressed(key, scancode, is_repeat)
    if states[current_state].keypressed then
        states[current_state]:keypressed(key, scancode, is_repeat)
    end
end

function love.mousereleased(x, y, button)
    if states[current_state].mousereleased then
        states[current_state]:mousereleased(x / settings.pixel_scale, y / settings.pixel_scale, button)
    end
end

function love.wheelmoved(xdiff, ydiff)
    if states[current_state].wheelmoved then
        local x,y = love.mouse.getPosition()
        states[current_state]:wheelmoved(x / settings.pixel_scale, y / settings.pixel_scale, xdiff, ydiff)
    end
end

function love.resize(width, height)
    if states[current_state].resize then
        states[current_state]:resize(width / settings.pixel_scale, height / settings.pixel_scale)
    end
end

function love.update(dt)
    local w,h = global_fns.get_viewport_dims()
    if w ~= old_w or h ~= old_h then
        old_w = w
        old_h = h

        if states[current_state].resize then
            states[current_state]:resize(w, h)
        end
    end

    local x,y = love.mouse.getPosition()
    local next_state, param = states[current_state]:update(dt, x / settings.pixel_scale, y / settings.pixel_scale)
    if not transition.active then
        gui.in_transition = false
        if next_state then
            transition.timer = 2
            transition.next = next_state
            transition.active = true
            local src = love.audio.newSource(resources.sound_data.transition)
            src:setVolume(settings.sound_volume * 0.01)
            love.audio.play(src)
        end
    else
        gui.in_transition = true
        if transition.timer >= 1 and transition.timer - dt < 1 then
            current_state = transition.next
            states[current_state]:init(param)
        end

        if transition.timer > 0 then
            transition.timer = transition.timer - dt
            if transition.timer < 0 then
                transition.timer = 0
                transition.next = nil
                transition.active = false
            end
        end
    end

    music_change_timer = music_change_timer + dt
    if current_music ~= nil then
        if not current_music.source:isPlaying() then
            global_fns.set_music(states[current_state]:get_music())
        end
    end
end

function love.draw()
    love.graphics.push()
    love.graphics.scale(settings.pixel_scale)
    states[current_state]:draw()

    if current_music and music_change_timer < 10 then
        local w,h = global_fns.get_viewport_dims()
        love.graphics.setColor(0, 0.03, 0.18, math.max(math.min(music_change_timer, 1) - math.max(music_change_timer - 9, 0), 0) * 0.5)
        love.graphics.rectangle("fill", 0, h - 20, w, 20)
        love.graphics.setColor(1, 1, 1, math.max(math.min(music_change_timer, 1) - math.max(music_change_timer - 9, 0), 0))
        love.graphics.draw(resources.textures.music_icon, 2, h - 18)
        love.graphics.draw(music_text, 24, h - 16)
        love.graphics.setColor(1, 1, 1, 1)
    end
    love.graphics.pop()

    if transition.timer > 0 then
        local width,height = love.window.getMode()
        local scale = math.max(width, height) / resources.textures.transition:getWidth()

        love.graphics.scale(scale)
        love.graphics.setShader(resources.shaders.transition_shader)
        resources.shaders.transition_shader:send("time", transition.timer)
        love.graphics.draw(resources.textures.transition, 0, 0)
        love.graphics.setShader()
    end
end
