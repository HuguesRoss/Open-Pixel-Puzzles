puzzle = { }
local metacalls = {
    getpiece = function(self, id)
        return self.piece_lookup[id]
    end,

    update = function(self, dt)
        if self.solved then
            self.solved_timer = self.solved_timer + dt
        end
    end,

    draw = function(self)
        love.graphics.setColor(self.canvas_colors[3])
        love.graphics.rectangle("fill", self.canvas_left - 1, self.canvas_top - 1, self.width * self.tile_width + 2, self.height * self.tile_height + 2)
        for _,slot in ipairs(self.slots) do
            love.graphics.setColor(self.canvas_colors[1 + (slot.canvas_x + slot.canvas_y) % 2])
            love.graphics.rectangle("fill", slot.x, slot.y, self.tile_width, self.tile_height)
        end

        if settings.puzzle_overlay then
            love.graphics.setColor(1, 1, 1, 0.4)
            love.graphics.draw(self.texture, self.canvas_left, self.canvas_top)
        end

        love.graphics.setShader(resources.shaders.piece_shader)
        love.graphics.setColor(0, 0, 0)
        for _,piece in ipairs(self.pieces) do
            if piece.placed then
                resources.shaders.piece_shader:send("add_color", { 0, 0, 0, 0 })
                resources.shaders.piece_shader:send("mask", piece.mask.outline)
                love.graphics.draw(piece.mesh, piece.x, piece.y)
            end
        end
        love.graphics.setColor(1, 1, 1)
        for _,piece in ipairs(self.pieces) do
            if piece.placed then
                local brightness = 0
                if self.solved then
                    brightness = math.max(1 - (self.solved_timer * 0.4), 0)
                end
                resources.shaders.piece_shader:send("add_color", { brightness, brightness, brightness, 0 })
                resources.shaders.piece_shader:send("mask", piece.mask.fill)
                love.graphics.draw(piece.mesh, piece.x, piece.y)

                if debug_mode.enabled then
                    -- Group debugging code
                    love.graphics.setShader()
                    love.graphics.push()
                    love.graphics.scale(1 / settings.pixel_scale, 1 / settings.pixel_scale)
                    self.debug_text:set(tostring(piece.group_logical_x) .. " " .. tostring(piece.group_logical_y))
                    love.graphics.draw(self.debug_text, (piece.x + self.tile_width * 0.45) * settings.pixel_scale, (piece.y + self.tile_height * 0.45) * settings.pixel_scale)
                    love.graphics.pop()
                    love.graphics.setShader(resources.shaders.piece_shader)
                end
            end
        end
        love.graphics.setShader()
    end,

    draw_piece = function(self, piece, x, y, shadow)
        local r,g,b,a = love.graphics.getColor()

        if shadow then
            resources.shaders.piece_shader:send("mask", piece.mask.fill)
            love.graphics.setColor(0, 0, 0, 0.35)
            love.graphics.draw(piece.mesh, x + (self.tile_width * 0.2), y + (self.tile_height * 0.2))
        end

        resources.shaders.piece_shader:send("mask", piece.mask.outline)
        resources.shaders.piece_shader:send("add_color", { 0, 0, 0, 0 })
        love.graphics.setColor(0, 0, 0)
        love.graphics.draw(piece.mesh, x, y)

        resources.shaders.piece_shader:send("mask", piece.mask.fill)
        resources.shaders.piece_shader:send("add_color", { 0, 0, 0, 0 })
        love.graphics.setColor(r, g, b, a)
        love.graphics.draw(piece.mesh, x, y)
    end,

    validate = function(self)
        self.solved = true
        for id,slot in ipairs(self.slots) do
            if slot.piece ~= id then
                self.solved = false
                return
            end
        end
    end,

    get_group_position = function(self, group_id)
        local group = self.groups[group_id]
        local piece = self:getpiece(group.pieces[1])
        return piece.x - piece.group_x, piece.y - piece.group_y
    end,

    break_group = function(self, group_id, piece_id)
        debug_mode.print("BREAK GROUP " .. tostring(group_id))

        local group = self.groups[group_id]
        local group_last = self.groups[#self.groups]
        if group ~= group_last then
            self.groups[group_id] = group_last
            for _,v in ipairs(group_last.pieces) do
                local piece = self:getpiece(v)
                piece.group_id = group_id
            end
        end
        self.groups[#self.groups] = nil

        local all_remaining_pieces = {}
        local remaining_pieces = {}

        for _,v in ipairs(group.pieces) do
            local piece = self:getpiece(v)
            piece.group_id = 0
            piece.group_x = 0
            piece.group_y = 0

            if piece.id ~= piece_id then
                local piece_data = {
                    piece = piece,
                    x = piece.group_logical_x,
                    y = piece.group_logical_y,
                }
                table.insert(all_remaining_pieces, piece_data)
                table.insert(remaining_pieces, piece_data)
            end

            piece.group_logical_x = 0
            piece.group_logical_y = 0
        end

        -- Re-form groups
        while #remaining_pieces > 0 do
            local piece = remaining_pieces[#remaining_pieces]
            for _,v in ipairs(all_remaining_pieces) do
                local xo = v.x - piece.x
                local yo = v.y - piece.y
                if math.abs(xo) + math.abs(yo) == 1 then -- Pieces are adjacent
                    debug_mode.print("CONNECTING " .. tostring(piece.x) .. " " .. tostring(piece.y) .. " TO " .. tostring(v.x) .. " " .. tostring(v.y))
                    self:group_pieces(piece.piece.id, v.piece.id, xo, yo)
                end
            end

            remaining_pieces[#remaining_pieces] = nil
        end
    end,

    merge_groups = function(self, group_id_a, group_id_b, logical_diff_x, logical_diff_y)
        local group_a = self.groups[group_id_a]
        local group_b = self.groups[group_id_b]
        local group_last = self.groups[#self.groups]

        local a_x, a_y = self:get_group_position(group_id_a)
        local b_x, b_y = self:get_group_position(group_id_b)

        local diff_x = a_x - b_x
        local diff_y = a_y - b_y

        if group_a ~= group_last then
            self.groups[group_id_a] = group_last
            for _,v in ipairs(group_last.pieces) do
                local piece = self:getpiece(v)
                piece.group_id = group_id_a
            end

            -- If we just swapped b into a's slot, we need to update the local id
            if group_last == group_b then
                group_id_b = group_id_a
            end
        end
        debug_mode.print("REMOVING GROUP " .. tostring(group_id_a) .. "... (" .. tostring(#self.groups - 1) .. " REMAIN)")
        table.remove(self.groups, #self.groups)

        for _,v in ipairs(group_a.pieces) do
            local piece = self:getpiece(v)
            piece.group_id = group_id_b
            piece.group_x = piece.group_x + diff_x
            piece.group_y = piece.group_y + diff_y
            piece.group_logical_x = piece.group_logical_x + logical_diff_x
            piece.group_logical_y = piece.group_logical_y + logical_diff_y
            table.insert(group_b.pieces, v)
        end
    end,

    group_pieces = function(self, piece_id_a, piece_id_b, x_offset, y_offset)
        local piece_a = self:getpiece(piece_id_a)
        local piece_b = self:getpiece(piece_id_b)

        if piece_a.group_id == 0 then
            if piece_b.group_id == 0 then
                table.insert(self.groups, {
                    pieces = {
                        piece_a.id,
                        piece_b.id,
                    },
                })
                piece_a.group_id = #self.groups
                piece_a.group_logical_x = 0
                piece_a.group_logical_y = 0
                piece_b.group_id = #self.groups
                piece_b.group_x = piece_b.x - piece_a.x
                piece_b.group_y = piece_b.y - piece_a.y
                piece_b.group_logical_x = piece_a.group_logical_x + x_offset
                piece_b.group_logical_y = piece_a.group_logical_y + y_offset
                debug_mode.print("CREATE GROUP " .. tostring(piece_b.group_id) .. "...")
            else
                debug_mode.print("ADD TO GROUP " .. tostring(piece_b.group_id) .. "...")
                -- Add a to b's group
                table.insert(self.groups[piece_b.group_id].pieces, piece_a.id)
                piece_a.group_id = piece_b.group_id
                piece_a.group_x = piece_b.group_x + piece_a.x - piece_b.x
                piece_a.group_y = piece_b.group_y + piece_a.y - piece_b.y
                piece_a.group_logical_x = piece_b.group_logical_x - x_offset
                piece_a.group_logical_y = piece_b.group_logical_y - y_offset
            end
        elseif piece_b.group_id == 0 then
                debug_mode.print("ADD TO GROUP " .. tostring(piece_a.group_id) .. "...")
            -- Add b to a's group
            table.insert(self.groups[piece_a.group_id].pieces, piece_b.id)
            piece_b.group_id = piece_a.group_id
            piece_b.group_x = piece_a.group_x + piece_b.x - piece_a.x
            piece_b.group_y = piece_a.group_y + piece_b.y - piece_a.y
            piece_b.group_logical_x = piece_a.group_logical_x + x_offset
            piece_b.group_logical_y = piece_a.group_logical_y + y_offset
        else
            if piece_a.group_id ~= piece_b.group_id then
                debug_mode.print("MERGE GROUPS " .. tostring(piece_a.group_id) .. ", " .. tostring(piece_b.group_id))
                -- Merge groups
                self:merge_groups(piece_a.group_id, piece_b.group_id, (piece_b.group_logical_x - piece_a.group_logical_x) - x_offset, (piece_b.group_logical_y - piece_a.group_logical_y) - y_offset)
            end
        end
    end,

    piecefits = function(self, slot_id, slot, piece)
        if piece.top == 0 then
            if slot.canvas_y ~= 0 then
                return false
            end
        else
            if slot.canvas_y == 0 then
                return false
            end
            local adj = self:getpiece(self.slots[slot_id - self.width].piece)
            if adj and adj.bottom == piece.top then
                return false
            end
        end

        if piece.bottom == 0 then
            if slot.canvas_y ~= self.height - 1 then
                return false
            end
        else
            if slot.canvas_y == self.height - 1 then
                return false
            end
            local adj = self:getpiece(self.slots[slot_id + self.width].piece)
            if adj and adj.top == piece.bottom then
                return false
            end
        end

        if piece.left == 0 then
            if slot.canvas_x ~= 0 then
                return false
            end
        else
            if slot.canvas_x == 0 then
                return false
            end
            local adj = self:getpiece(self.slots[slot_id - 1].piece)
            if adj and adj.right == piece.left then
                return false
            end
        end

        if piece.right == 0 then
            if slot.canvas_x ~= self.width - 1 then
                return false
            end
        else
            if slot.canvas_x == self.width - 1 then
                return false
            end
            local adj = self:getpiece(self.slots[slot_id + 1].piece)
            if adj and adj.left == piece.right then
                return false
            end
        end

        return slot.piece == 0
    end,

    place_piece = function(self, piece_id, slot_id)
        local piece = self:getpiece(piece_id)
        piece.placed = true
        piece.slot_id = slot_id
        self.slots[slot_id].piece = piece.id
        piece.x = self.slots[slot_id].x
        piece.y = self.slots[slot_id].y

        if piece.top ~= 0 then
            local adj = self:getpiece(self.slots[slot_id - self.width].piece)
            if adj then
                self:group_pieces(piece.id, adj.id, 0, -1)
            end
        end

        if piece.bottom ~= 0 then
            local adj = self:getpiece(self.slots[slot_id + self.width].piece)
            if adj then
                self:group_pieces(piece.id, adj.id, 0, 1)
            end
        end

        if piece.left ~= 0 then
            local adj = self:getpiece(self.slots[slot_id - 1].piece)
            if adj then
                self:group_pieces(piece.id, adj.id, -1, 0)
            end
        end

        if piece.right ~= 0 then
            local adj = self:getpiece(self.slots[slot_id + 1].piece)
            if adj then
                self:group_pieces(piece.id, adj.id, 1, 0)
            end
        end
    end,

    lift_pieces = function(self, pieces)
        for _,piece_id in ipairs(pieces) do
            for _,slot in ipairs(self.slots) do
                if slot.piece == piece_id then
                    local piece = self:getpiece(slot.piece)
                    piece.slot_id = 0
                    slot.piece = 0
                    break
                end
            end

            self:getpiece(piece_id).placed = false
        end
    end,
}

local function getmask(p, top, bottom, left, right)
    local index = top + (bottom * 4) + (left * 16) + (right * 64)

    if not p.masks[index] then
        local mask_width = p.tile_width + (p.tooth_length * 2) + 2
        local mask_height = p.tile_height + (p.tooth_length * 2) + 2

        -- left/right edges of the top & bottom sides
        local left_side_length = math.floor((mask_width - p.tooth_width) * 0.5)
        local right_side_length = math.ceil((mask_width + p.tooth_width) * 0.5)

        -- top/bottom edges of the left & right sides
        local upper_side_height = math.floor((mask_height - p.tooth_width) * 0.5)
        local lower_side_height = math.ceil((mask_height + p.tooth_width) * 0.5)

        local outline = love.image.newImageData(mask_width, mask_height)
        local fill = love.image.newImageData(mask_width, mask_height)

        for y=0,mask_height-1 do
            for x=0,mask_width-1 do
                -- Basic rectangle fill $ outline
                if x > p.tooth_length and x < mask_width - (p.tooth_length + 1) and
                   y > p.tooth_length and y < mask_height - (p.tooth_length + 1) then
                    fill:setPixel(x, y, 1, 1, 1, 1)
                elseif (y >= p.tooth_length and y <= mask_height - (p.tooth_length + 1) and (x == p.tooth_length or x == mask_width - (p.tooth_length + 1))) or
                       (x >= p.tooth_length and x <= mask_width - (p.tooth_length + 1) and (y == p.tooth_length or y == mask_height - (p.tooth_length + 1))) then
                    outline:setPixel(x, y, 1, 1, 1, 1)
                end

                -- Top & bottom teeth
                if x >= left_side_length - 1 and x < right_side_length + 1 then
                    local is_side_border = x == left_side_length - 1 or x == right_side_length
                    local is_side_inner_border = x == left_side_length or x == right_side_length - 1

                    -- Top teeth
                    if top == 1 and y < p.tooth_length + 1 then
                        if is_side_border or y == 0 then
                            outline:setPixel(x, y, 1, 1, 1, 1)
                        else
                            fill:setPixel(x, y, 1, 1, 1, 1)
                            outline:setPixel(x, y, 0, 0, 0, 0)
                        end
                    elseif top == 2 and not is_side_border and y >= p.tooth_length and y <= p.tooth_length * 2 then
                        if is_side_inner_border or y == p.tooth_length * 2 then
                            fill:setPixel(x, y, 0, 0, 0, 0)
                            outline:setPixel(x, y, 1, 1, 1, 1)
                        else
                            fill:setPixel(x, y, 0, 0, 0, 0)
                            outline:setPixel(x, y, 0, 0, 0, 0)
                        end
                    end

                    -- Bottom teeth
                    if bottom == 1 and y >= mask_height - p.tooth_length - 1 then
                        if is_side_border or y == mask_height - 1 then
                            outline:setPixel(x, y, 1, 1, 1, 1)
                        else
                            fill:setPixel(x, y, 1, 1, 1, 1)
                            outline:setPixel(x, y, 0, 0, 0, 0)
                        end
                    elseif bottom == 2 and not is_side_border and y <= mask_height - p.tooth_length - 1 and y > mask_height - (p.tooth_length * 2) - 2 then
                        if is_side_inner_border or y == mask_height - (p.tooth_length * 2) - 1 then
                            fill:setPixel(x, y, 0, 0, 0, 0)
                            outline:setPixel(x, y, 1, 1, 1, 1)
                        else
                            fill:setPixel(x, y, 0, 0, 0, 0)
                            outline:setPixel(x, y, 0, 0, 0, 0)
                        end
                    end
                end

                -- Left & Right teeth
                if y >= upper_side_height - 1 and y < lower_side_height + 1 then
                    local is_side_border = y == upper_side_height - 1 or y == lower_side_height
                    local is_side_inner_border = y == upper_side_height or y == lower_side_height - 1

                    -- Left teeth
                    if left == 1 and x < p.tooth_length + 1 then
                        if is_side_border or x == 0 then
                            outline:setPixel(x, y, 1, 1, 1, 1)
                        else
                            fill:setPixel(x, y, 1, 1, 1, 1)
                            outline:setPixel(x, y, 0, 0, 0, 0)
                        end
                    elseif left == 2 and not is_side_border and x >= p.tooth_length and x <= p.tooth_length * 2 then
                        if is_side_inner_border or x == p.tooth_length * 2 then
                            fill:setPixel(x, y, 0, 0, 0, 0)
                            outline:setPixel(x, y, 1, 1, 1, 1)
                        else
                            fill:setPixel(x, y, 0, 0, 0, 0)
                            outline:setPixel(x, y, 0, 0, 0, 0)
                        end
                    end

                    -- Right teeth
                    if right == 1 and x >= mask_width - p.tooth_length - 1 then
                        if is_side_border or x == mask_width - 1 then
                            outline:setPixel(x, y, 1, 1, 1, 1)
                        else
                            fill:setPixel(x, y, 1, 1, 1, 1)
                            outline:setPixel(x, y, 0, 0, 0, 0)
                        end
                    elseif right == 2 and not is_side_border and x <= mask_width - p.tooth_length - 1 and x > mask_width - (p.tooth_length * 2) - 2 then
                        if is_side_inner_border or x == mask_width - (p.tooth_length * 2) - 1 then
                            fill:setPixel(x, y, 0, 0, 0, 0)
                            outline:setPixel(x, y, 1, 1, 1, 1)
                        else
                            fill:setPixel(x, y, 0, 0, 0, 0)
                            outline:setPixel(x, y, 0, 0, 0, 0)
                        end
                    end
                end
            end
        end
        p.masks[index] = {
            outline = love.graphics.newImage(outline),
            fill = love.graphics.newImage(fill),
        }
    end

    return p.masks[index]
end

local function createmesh(p, x, y)
    local i_width, i_height = p.texture:getDimensions()
    local mesh_width = p.tile_width + p.tooth_length + 1
    local mesh_height = p.tile_height + p.tooth_length + 1
    local mesh = love.graphics.newMesh(
        {
            { "VertexPosition", "float", 2 },
            { "SimpleTexCoord", "float", 2 },
            { "VertexTexCoord", "float", 2 },
        },
        {
            { -p.tooth_length - 1, -p.tooth_length - 1,    0, 0,    (x - p.tooth_length - 1) / i_width, (y - p.tooth_length - 1) / i_height},
            {  mesh_width,         -p.tooth_length - 1,    1, 0,    (x + mesh_width) / i_width,         (y - p.tooth_length - 1) / i_height},
            {  mesh_width,          mesh_height,           1, 1,    (x + mesh_width) / i_width,        (y + mesh_height) / i_height},

            { -p.tooth_length - 1, -p.tooth_length - 1,    0, 0,    (x - p.tooth_length - 1) / i_width, (y - p.tooth_length - 1) / i_height},
            { mesh_width,           mesh_height,           1, 1,    (x + mesh_width) / i_width,         (y + mesh_height) / i_height},
            { -p.tooth_length - 1,  mesh_height,           0, 1,    (x - p.tooth_length - 1) / i_width, (y + mesh_height) / i_height},
        }, "triangles")
    mesh:setTexture(p.texture)

    return mesh
end

function puzzle.new(params, index, scene_left, scene_right, scene_top, scene_bottom)
    debug_mode.print("Loading puzzle...")

    local texture = love.graphics.newImage(params.image)
    local i_width, i_height = texture:getDimensions()
    local tile_width = params.piece_sizes[index or 1].width
    local tile_height = params.piece_sizes[index or 1].height
    local tooth_length = params.tooth_sizes[index or 1].length
    local tooth_width = params.tooth_sizes[index or 1].width

    local p = {
        texture = texture,
        solved = false,
        solved_timer = 0,

        width = math.floor(i_width / tile_width),
        height = math.floor(i_height / tile_height),
        tile_width = tile_width,
        tile_height = tile_height,
        tooth_length = tooth_length,
        tooth_width = tooth_width,

        pieces = {},
        piece_lookup = {},
        groups = {},
        slots = {},
        masks = {},
        canvas_colors = params.canvas_colors,
    }
    if not p.canvas_colors[1] then
        p.canvas_colors[1] = { 0.6, 0.6, 0.5 }
    end
    if not p.canvas_colors[2] then
        p.canvas_colors[2] = { 0.8, 0.8, 0.7 }
    end
    if not p.canvas_colors[3] then
        p.canvas_colors[3] = { 0, 0, 0 }
    end
    if debug_mode.enabled then
        p.debug_text = love.graphics.newText(resources.fonts.default, "0 0")
    end

    p.canvas_left = math.floor(-(p.width * 0.5 * tile_width))
    p.canvas_top = math.floor(-(p.height * 0.5 * tile_height))
    p.canvas_right = math.floor((p.width * 0.5 * tile_width))
    p.canvas_bottom = math.floor((p.height * 0.5 * tile_height))

    if i_width + (p.tile_width * 3.2) > scene_right - scene_left then
        scene_right = p.canvas_right + (p.tile_width * 1.6)
        scene_left = p.canvas_left - (p.tile_width * 1.6)
    end
    if i_height > scene_bottom - scene_top then
        scene_bottom = p.canvas_bottom + (p.tile_height * 1.6)
        scene_top = p.canvas_top - (p.tile_height * 1.6)
    end

    local connections = {}

    for j=0,p.height-1 do
        for i=0,p.width-1 do
            if not connections[j * p.width + i] then
                connections[j * p.width + i] = {
                    h = love.math.random(1, 2),
                    v = love.math.random(1, 2),
                }
            end

            table.insert(p.slots, {
                x = i * p.tile_width + p.canvas_left,
                y = j * p.tile_height + p.canvas_top,
                canvas_x = i,
                canvas_y = j,
                piece = 0,
            })

            local x = 0
            local y = 0
            local attempt = 1
            while x > p.canvas_left - p.tile_width * 1.2 and x < p.canvas_right + p.tile_width * 0.2 and
                  y > p.canvas_top - p.tile_height * 1.2 and y < p.canvas_bottom + p.tile_height * 0.2
                  or (x < scene_left + 64 and y < scene_top + 64) -- Back button
                  do
                      x = love.math.random(scene_left + p.tile_width * 0.2, scene_right - p.tile_width * 1.2)
                      y = love.math.random(scene_top + p.tile_height * 0.2, scene_bottom - p.tile_height * 1.2)
                      attempt = attempt + 1

                      if attempt > 1000 then
                          break
                      end
            end

            local top = j == 0 and 0 or 3 - connections[(j - 1) * p.width + i].v
            local bottom = j == p.height - 1 and 0 or connections[j * p.width + i].v
            local left = i == 0 and 0 or 3 - connections[j * p.width + i - 1].h
            local right = i == p.width - 1 and 0 or connections[j * p.width + i].h

            local piece = {
                id = #p.pieces + 1,
                group_id = 0,
                x = x,
                y = y,
                group_x = 0,
                group_y = 0,
                group_logical_x = 0,
                group_logical_y = 0,
                top = top,
                bottom = bottom,
                left = left,
                right = right,
                mesh = createmesh(p, i * p.tile_width, j * p.tile_height),
                mask = getmask(p, top, bottom, left, right),
                placed = false,
            }

            table.insert(p.piece_lookup, piece)
            table.insert(p.pieces, piece)
        end
    end

    debug_mode.print("Done loading puzzle")

    setmetatable(p, { __index = metacalls, })
    return p, scene_left, scene_right, scene_top, scene_bottom
end

function puzzle.load_params(folder)
    local filepath = folder .. "/puzzle.conf"
    local raw_params,err = serializer.load_params(filepath)
    if err then
        return
    end

    local params = {
        piece_sizes = {},
        tooth_sizes = {},
        canvas_colors = {},
    }
    local license_lookup = {
        ["CC BY-SA 4.0"] = { "by", "sa" },
        ["CC BY-SA 3.0"] = { "by", "sa" },
        ["CC BY 4.0"] = "by",
        ["CC BY 3.0"] = "by",
        ["CC0"] = "zero",
    }
    for k,v in pairs(raw_params) do
        if k == "piece_sizes" then
            for size_txt in v:gmatch("[^,]+") do
                local _,_,w,h = size_txt:find("(%d+)x(%d+)")
                local size = {
                    width = tonumber(w),
                    height = tonumber(h),
                }

                if size.width and size.height then
                    table.insert(params.piece_sizes, size)
                end
            end
        elseif k == "tooth_sizes" then
            for size_txt in v:gmatch("[^,]+") do
                local _,_,l,w = size_txt:find("(%d+)x(%d+)")
                local size = {
                    length = tonumber(l),
                    width = tonumber(w),
                }

                if size.length and size.width then
                    table.insert(params.tooth_sizes, size)
                end
            end
        elseif k == "image" then
            params[k] = folder .. "/" .. v
        elseif k == "preview_region" then
            local _,_,x,y,w,h = v:find("(%d+),(%d+):(%d+)x(%d+)")
            params[k] = {
                x = x,
                y = y,
                width = w,
                height = h,
            }
        elseif k == "bg_color" then
            params[k] = {}
            params[k][1],params[k][2],params[k][3] = math.tocolor(v)
        elseif k == "canvas_colors" then
            for color_txt in v:gmatch("[^,]+") do
                local color = {}
                color[1],color[2],color[3] = math.tocolor(color_txt)
                table.insert(params[k], color)
            end
        elseif k == "license" then
            params[k] = license_lookup[v]
        else
            params[k] = tonumber(v) or v
        end
    end
    return params
end
