function love.conf(t)
    t.identity = "open_pixel_puzzles"

    t.window.title = "Open Pixel Puzzles"
    -- TODO
    t.window.icon = "skins/default/icon.png"
    t.window.minwidth = 1280
    t.window.minheight = 720
    t.window.resizable = true

    t.modules.data = false
    t.modules.joystick = false
    t.modules.keyboard = false
    t.modules.physics = false
    t.modules.thread = false
    t.modules.touch = false
    t.modules.video = false
end
