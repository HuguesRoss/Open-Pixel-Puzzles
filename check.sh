#!/bin/sh
luacheck $@\
 --std=lua54+love\
 --no-max-line-length\
 --exclude-files="extern"\
