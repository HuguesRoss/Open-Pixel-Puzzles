# Open Pixel Puzzles
Open Pixel Puzzles is an open-source jigsaw puzzle game optimized for pixel art. The goal is to offer a simple but polished experience for both puzzle-lovers and casual players alike.

## Usage
Currently this game can only be operated with the mouse:
- Left Click & Drag: Pick up a piece or group of connected pieces, release to drop
- Right Click & Drag: Pick up a single piece (breaking any connections it has), release to drop
- Scroll Wheel: Zoom in / out

When the puzzle area is larger than the window, you can also pan the view by clicking and dragging the middle mouse button.

More control options will be added in future releases.

## Contact
For questions, requests, and other communication regarding this work, you can contact the original creator at hugues.ross@gmail.com
